import{e as N,f as d,_ as V,o as k,c as w,a as n,h as l,b as z,w as q,T as H,i as $,v as L,j as s,F as J,d as K,n as M,t as O,p as P,k as Q}from"./index-dc362556.js";function B(){return[{key:"a",index:1,class:{"grid-item1":!0},style:{"background-color":"rgb(239,52,41)"}},{key:"b",index:2,class:{"grid-item2":!0},style:{"background-color":"rgb(246,143,37)"}},{key:"c",index:3,class:{"grid-item3":!0},style:{"background-color":"rgb(75,168,70)"}},{key:"d",index:4,class:{"grid-item4":!0},style:{"background-color":"rgb(4,118,194)"}},{key:"e",index:5,class:{"grid-item5":!0},style:{"background-color":"rgb(192,119,175)"}},{key:"f",index:6,class:{"grid-item6":!0},style:{"background-color":"rgb(248,210,157)"}},{key:"g",index:7,class:{"grid-item7":!0},style:{"background-color":"rgb(180,168,127)"}},{key:"h",index:8,class:{"grid-item8":!0},style:{"background-color":"rgb(208,228,168)"}},{key:"j",index:9,class:{"grid-item9":!0},style:{"background-color":"rgb(77,199,236)"}}]}let U=100;function W(){return{key:U+++"",class:{},style:{"background-color":"#fff"}}}const X=N({setup(){const e=d(0),t=d("css-grid-box-row-column"),a=d([]),c=d(void 0),v=d(!0),b=d(!1);function i(o){v.value=o==="rowColumnLinesActive",b.value=o==="gridTemplateColumnsActive"}function A(o){i("rowColumnLinesActive"),t.value="css-grid-box-row-column",c.value=o;let r=[],p=0,G=B();for(let f=1;f<8;f++)for(let C=1;C<8;C++)r.push(f%2|C%2?W():G[p++]);a.value=r,console.log(a.value)}A(void 0);function g(o){for(let r=0,p=0;r<a.value.length;r++)r!==0&&r%7===0&&p++,a.value[r].class["css-grid-high-light-animation"]=o(p,r%7)}const x=d(!1),y=d(!1),h=d(!1),T=d(!1);function m(o){x.value=o==="rowLinesActive",y.value=o==="columnLinesActive",h.value=o==="columnActive",T.value=o==="rowActive"}function E(){m("rowLinesActive"),g(()=>!1),setTimeout(()=>{g(o=>o%2===0)})}function F(){m("columnLinesActive"),g(()=>!1),setTimeout(()=>{g((o,r)=>r%2===0)})}function j(){m("rowActive"),g(()=>!1),setTimeout(()=>{g((o,r)=>o%2===1)})}function S(){m("columnActive"),g(()=>!1),setTimeout(()=>{g((o,r)=>r%2===1)})}function D(){g(()=>!1),a.value=B(),c.value=5,i("gridTemplateColumnsActive"),t.value="css-grid-box-grid-template-columns1",console.log(a.value)}function I(o){return c.value===o?"button-active":""}function R(o,r){c.value=o,typeof r=="string"?t.value=r:r()}return{rowColumnLinesActive:v,gridTemplateColumnsActive:b,activeIndex:c,count:e,arr:a,rowLinesActive:x,columnLinesActive:y,columnActive:h,rowActive:T,boxClass:t,rowColumnLinesTest:A,highlightRowLines:E,highlightColumnLines:F,highlightRows:j,highlightColumns:S,gridTemplateColumnsTest:D,isActive:I,itemClick:R}}});const u=e=>(P("data-v-01458cb9"),e=e(),Q(),e),Y={class:"grid-test padding-box-100"},Z={class:"css-grid-buttons"},_=u(()=>n("br",null,null,-1)),ee={class:"css-grid-buttons"},te=u(()=>n("strong",null,"点击下方样式进行切换",-1)),ne={style:{"max-height":"40vh",overflow:"auto"}},se=u(()=>n("strong",null,"/**  grid-template-columns: repeat(3, 90px);  **/ ",-1)),ie=u(()=>n("strong",null,"/**   grid-template-columns: repeat(auto-fill, 40px);  **/ ",-1)),oe=u(()=>n("strong",null,"/**   grid-template-columns:30px 1fr 2fr;  **/ ",-1)),le=u(()=>n("strong",null,"/**    grid-template-columns:30% 30% 40%;  **/ ",-1)),re=u(()=>n("strong",null,"/**   grid-template-columns: repeat(3, auto);  **/ ",-1)),ue=u(()=>n("strong",null,"/**   justify-content: left; align-content: center;  **/ ",-1)),de=u(()=>n("strong",null,"/**   grid-auto-flow: column;  **/",-1)),ge=u(()=>n("strong",null,"/**   grid-auto-flow: row dense;  **/",-1)),ae=u(()=>n("strong",null,"/**   grid-auto-flow: row;  **/",-1)),ce=u(()=>n("strong",null,"/**   justify-items: left;align-items 同理  **/",-1)),me=u(()=>n("strong",null,"/**   grid-auto-rows: 45px;grid-auto-columns: 45px;  **/",-1)),pe=u(()=>n("strong",null,"/**   grid-column-start: 5;grid-column-end: 6;  **/",-1)),ve=u(()=>n("strong",null,"/**   grid-row-start: 4;grid-row-end: 5;  **/",-1)),be=u(()=>n("strong",null,"/**   grid-column: 1 / 3;grid-row: 1 / 3;  **/",-1)),fe=u(()=>n("strong",null,"/**   grid-column: 1 / 3;grid-row: 1 / 3;  **/",-1));function Ce(e,t,a,c,v,b){return k(),w("div",Y,[n("div",Z,[n("button",{class:l(e.rowColumnLinesActive?"button-active":""),onClick:t[0]||(t[0]=i=>e.rowColumnLinesTest(1))},"行列及网格线区分实验 ",2),n("button",{class:l(e.gridTemplateColumnsActive?"button-active":""),onClick:t[1]||(t[1]=(...i)=>e.gridTemplateColumnsTest&&e.gridTemplateColumnsTest(...i))}," grid-template-columns实验 ",2)]),_,z(H,{class:l([e.boxClass,"css-grid-box"]),name:"flip-list",tag:"div"},{default:q(()=>[(k(!0),w(J,null,K(e.arr,i=>(k(),w("div",{key:i.key,style:M(i.style),class:l(i.class)},O(i.index||""),7))),128))]),_:1},8,["class"]),n("div",ee,[te,$(n("div",null,[n("button",{class:l(e.isActive(0)),onClick:t[2]||(t[2]=i=>e.itemClick(0,e.highlightRows))},"高亮行",2),n("button",{class:l(e.isActive(1)),onClick:t[3]||(t[3]=i=>e.itemClick(1,e.highlightColumns))},"高亮列",2),n("button",{class:l(e.isActive(2)),onClick:t[4]||(t[4]=i=>e.itemClick(2,e.highlightRowLines))},"高亮行网格线",2),n("button",{class:l(e.isActive(3)),onClick:t[5]||(t[5]=i=>e.itemClick(3,e.highlightColumnLines))},"高亮列网格线",2)],512),[[L,e.rowColumnLinesActive]]),$(n("div",ne,[n("pre",{class:l(e.isActive(5)),onClick:t[6]||(t[6]=i=>e.itemClick(5,"css-grid-box-grid-template-columns1"))},[s("    "),se,s(`
    .css-grid-box-grid-template-columns1 {
        grid-template-columns: repeat(3, 90px);
        row-gap: 15px;
        column-gap: 15px;
    }
                `)],2),n("pre",{class:l(e.isActive(6)),onClick:t[7]||(t[7]=i=>e.itemClick(6,"css-grid-box-grid-template-columns2"))},[s("    "),ie,s(`
    .css-grid-box-grid-template-columns2 {
        grid-template-columns: repeat(auto-fill, 40px);
        row-gap: 15px;
        column-gap: 15px;
    }
                `)],2),n("pre",{class:l(e.isActive(7)),onClick:t[8]||(t[8]=i=>e.itemClick(7,"css-grid-box-grid-template-columns3"))},[s("    "),oe,s(`
    .css-grid-box-grid-template-columns3 {
        grid-template-columns:30px 1fr 2fr;
        row-gap: 15px;
        column-gap: 15px;
    }
                `)],2),n("pre",{class:l(e.isActive(8)),onClick:t[9]||(t[9]=i=>e.itemClick(8,"css-grid-box-grid-template-columns4"))},[s("    "),le,s(`
    .css-grid-box-grid-template-columns4 {
        grid-template-columns:30% 30% 40%;
        row-gap: 15px;
    }
                `)],2),n("pre",{class:l(e.isActive(9)),onClick:t[10]||(t[10]=i=>e.itemClick(9,"css-grid-box-grid-template-columns5"))},[s("    "),re,s(`
    .css-grid-box-grid-template-columns5 {
        grid-template-columns: repeat(3, auto);
        row-gap: 15px;
        column-gap: 15px;
    }
                `)],2),n("pre",{class:l(e.isActive(10)),onClick:t[11]||(t[11]=i=>e.itemClick(10,"css-grid-box-grid-template-columns6"))},[s("    "),ue,s(`
    .css-grid-box-grid-template-columns6 {
        grid-template-columns: repeat(3, 60px);
        grid-template-rows: repeat(3, 60px);
        justify-content: left;
        align-content: center;
    }
                `)],2),n("pre",{class:l(e.isActive(11)),onClick:t[12]||(t[12]=i=>e.itemClick(11,"css-grid-box-grid-template-columns8"))},[s("    "),de,s(`
    .css-grid-box-grid-template-columns8 {
        grid-template-columns: repeat(3, auto);
        grid-template-rows: repeat(3, auto);
        grid-auto-flow: column;
    }
                `)],2),n("pre",{class:l(e.isActive(12)),onClick:t[13]||(t[13]=i=>e.itemClick(12,"css-grid-box-grid-template-columns9"))},[s("    "),ge,s(`
    .css-grid-box-grid-template-columns9 {
        grid-template-columns: repeat(3, auto);
        grid-template-rows: repeat(3, auto);
        grid-auto-flow: row dense;
    }

    .css-grid-box-grid-template-columns9 .grid-item1,
    .css-grid-box-grid-template-columns9 .grid-item2{
        grid-row-start: 1;
        grid-row-end: 3;
    }
                `)],2),n("pre",{class:l(e.isActive(13)),onClick:t[14]||(t[14]=i=>e.itemClick(13,"css-grid-box-grid-template-columns10"))},[s("    "),ae,s(`
    .css-grid-box-grid-template-columns10 {
        grid-template-columns: repeat(3, auto);
        grid-template-rows: repeat(3, auto);
        grid-auto-flow: row;
    }

    .css-grid-box-grid-template-columns10 .grid-item1,
    .css-grid-box-grid-template-columns10 .grid-item2{
        grid-row-start: 1;
        grid-row-end: 3;
    }
                `)],2),n("pre",{class:l(e.isActive(14)),onClick:t[15]||(t[15]=i=>e.itemClick(14,"css-grid-box-grid-template-columns11"))},[s("    "),ce,s(`
    .css-grid-box-grid-template-columns11 {
        grid-template-columns: repeat(3, auto);
        grid-template-rows: repeat(3, auto);
        justify-items: left;
    }
                `)],2),n("pre",{class:l(e.isActive(15)),onClick:t[16]||(t[16]=i=>e.itemClick(15,"css-grid-box-grid-template-columns12"))},[s("    "),me,s(`
    `),pe,s(`
    `),ve,s(`
    .css-grid-box-grid-template-columns12 {
        grid-template-columns: repeat(3, 100px);
        grid-template-rows: repeat(3, 100px);
        grid-auto-rows: 45px;
        grid-auto-columns: 45px;
    }
    .css-grid-box-grid-template-columns12 .grid-item3 {
        grid-column-start: 5;
        grid-column-end: 6;
    }
    .css-grid-box-grid-template-columns12 .grid-item8 {
        grid-row-start: 4;
        grid-row-end: 5;
    }
                `)],2),n("pre",{class:l(e.isActive(16)),onClick:t[17]||(t[17]=i=>e.itemClick(16,"css-grid-box-grid-template-columns13"))},[s("    "),be,s(`
    .css-grid-box-grid-template-columns13 {
        grid-template-columns: repeat(4, auto);
        grid-template-rows: repeat(4, auto);
    }
    .css-grid-box-grid-template-columns13 .grid-item1 {
        grid-column: 1 / 3;
        grid-row: 1 / 3;
    }
    .css-grid-box-grid-template-columns13 .grid-item2 {
        grid-column: 3 / span 2;
        grid-row: 1;
    }
                `)],2),n("pre",{class:l(e.isActive(17)),onClick:t[18]||(t[18]=i=>e.itemClick(17,"css-grid-box-grid-template-columns14"))},[s("    "),fe,s(`
    .css-grid-box-grid-template-columns14 {
        grid-template-columns: repeat(3, 60px);
        grid-template-rows: repeat(3, 60px);
        justify-content: center;
        align-content: center;
    }
    .css-grid-box-grid-template-columns14 .grid-item1 {
        justify-self: left;
    }
    .css-grid-box-grid-template-columns14 .grid-item3 {
        align-self: start;
    }
    .css-grid-box-grid-template-columns14 .grid-item9 {
        place-self:end right;
    }
                `)],2)],512),[[L,e.gridTemplateColumnsActive]])])])}const we=V(X,[["render",Ce],["__scopeId","data-v-01458cb9"]]);export{we as default};
