import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import copy from "rollup-plugin-copy";
import {fileURLToPath} from "node:url";
import path from "path";

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    copy({
      targets: [
        {src: 'src/view/*', dest: 'public/vue-files'}
      ],
      hook: 'buildStart'
    }),
  ],
  base: '/code-example/',
  server: {
    port: 3009,
    host: '0.0.0.0'
  },
  resolve: {
    alias: [
      {find: '@', replacement: path.resolve(__dirname, './src')}
    ]
  },
})
