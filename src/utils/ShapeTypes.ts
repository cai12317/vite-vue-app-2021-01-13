import {Line} from "@/utils/Line";
import {Rect} from "@/utils/Rect";
import {Shape} from "@/utils/Shape";

export type ShapeTypeKey = 'Line' | 'Rect'

export const ShapeTypes = {
  'Line': Line,
  'Rect': Rect,
}

export function getShapeClass(type: ShapeTypeKey): typeof Shape {
  return ShapeTypes[type]
}
