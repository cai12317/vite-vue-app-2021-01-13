import {random} from 'lodash'


export function getColor16() {
  let str = '#'
  for (let i = 0; i < 6; i++) {
    let randomNum = random(48, 63)

    str += String.fromCharCode(randomNum > 57 ? randomNum + 7 : randomNum)
  }
  return str
}

