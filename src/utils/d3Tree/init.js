import {
  clearContainer,
  setContainerBaseStyle,
  appendHiddenSvg,
  getContainerSize,
  appendRootSvg,
  appendRootBox,
} from "./utils.js";
import {appendSwitchIconDef, appendArrowIconDef} from "./core/icon.js";
import {containerResizeObserver} from "./event/containerResizeObserver.js";

/**
 *  初始化tree配置 及容器
 * @param treeMind {TreeMind}
 */
export function initTreeMind(treeMind) {
  const {dom} = treeMind

  // 清理容器
  clearContainer(dom)
  // 设置容器基础样式
  setContainerBaseStyle(dom)
  // 添加用于计算样式的隐藏 svg
  const hiddenSvg = appendHiddenSvg(dom)
  // 获取外部容器尺寸
  const {width, height, boundingClientRect} = getContainerSize(dom)
  // 添加根节容器 svg
  const svg = appendRootSvg(dom, width, height)
  // 添加 defs 容器
  svg.append('defs')
  // 创建根容器
  const rootBox = appendRootBox(svg)
  // 添加 path 和 node 的根容器
  let pathBox = rootBox.append('g')
  let nodeBox = rootBox.append('g')

  // 监听容器尺寸变化
  containerResizeObserver(treeMind)

  // 添加 icon
  appendSwitchIconDef(svg)
  appendSwitchIconDef(svg, false)
  appendArrowIconDef(svg)

  return {
    svg,
    rootBox,
    pathBox,
    nodeBox,
    hiddenSvg,
    boundingClientRect,
  }
}
