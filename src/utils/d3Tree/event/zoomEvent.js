import {zoom} from "d3";
import {ROOT_BOX_CLASS} from "../core/constants.js";
import {EVENT_TYPE_ENUM} from "./eventEnum.js";
import {throttle} from "../utils.js";

/** 初始化 zoom 事件
 *
 * @param treeMind {TreeMind}
 * @param onZoom
 */
export function initZoomEvent(treeMind, onZoom) {

  const {svg, dispatch} = treeMind

  const rootBoxSelection = svg.select(ROOT_BOX_CLASS.selector)

  function zoomed({transform}) {
    rootBoxSelection.attr('transform', transform)
    onZoom(transform)
  }

  const zoomCall = zoom()
          .scaleExtent(treeMind.config.scaleExtent)
          .filter(event => {
            return treeMind.config.disabled ? true : event.altKey
          })
          .on("zoom", zoomed)
          .on("end", throttle(function (...args) {
            dispatch.emit(EVENT_TYPE_ENUM.ZOOM_END, ...args)
          }))

  svg.call(zoomCall)

  return zoomCall
}
