export class TreeDispatch {
  listeners

  constructor() {
    this.listeners = {}
  }

  on(type, listener) {
    this.listeners[type] = this.listeners[type] || []
    this.listeners[type].push(listener)
    return () => {
      const index = this.listeners[type].findIndex(v => v === listener)
      this.listeners[type].splice(index, 1)
    }
  }

  off(type, listener) {
    if (listener) {
      const index = this.listeners[type].findIndex(v => v === listener)
      this.listeners[type].splice(index, 1)
    } else {
      delete this.listeners[type]
    }
  }

  emit(type, ...args) {
    const listeners = this.listeners[type] || []
    const listenerRes = []
    for (const listener of listeners) {
      listenerRes.push(listener(...args))
    }
    return listenerRes
  }
}

export class TreeEvent {
  sourceEvent
  data
  node

  constructor(sourceEvent, node, data) {
    this.sourceEvent = sourceEvent
    this.node = node
    this.data = data
  }
}
