/**
 *
 * @param treeMind {TreeMind}
 */
export function containerResizeObserver(treeMind) {
  const {dom} = treeMind

  function resetSvg(rect) {
    console.log('resetSvg----')
    treeMind.boundingClientRect = rect
    treeMind.svg
            .attr('width', rect.width)
            .attr('height', rect.height)
            .attr('viewBox', `0 0 ${rect.width} ${rect.height}`)
  }

  let timeout

  function debounceResetSvg(...args) {
    if (timeout) {
      clearTimeout(timeout)
    }

    timeout = setTimeout(() => {
      resetSvg(...args)
      timeout = null
    }, 500)
  }

  const objResizeObserver = new ResizeObserver(function (entries) {
    console.log(entries)
    const entry = entries[0]
    const {contentRect} = entry
    debounceResetSvg(contentRect)
  })

  objResizeObserver.observe(dom)
}
