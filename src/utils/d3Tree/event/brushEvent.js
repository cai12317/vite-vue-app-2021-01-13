import {brush} from "d3";
import {PERMISSION_HANDLER_ENUM} from "../permission/permissionEnum.js";
import {EVENT_TYPE_ENUM} from "./eventEnum.js";

export function initBrushEvent(updateSelected) {
  return brush().filter(() => false).on('end', function ({selection}) {
    if (selection) {
      updateSelected(selection)
    }
  })
}

let start = false, startX = 0, startY = 0, brushFirst = true

/**
 *
 * @param treeMind {TreeMind}
 * @param brushCall
 */
export function initMouthEvent(treeMind, brushCall) {

  const {svg, permissionInterceptor, dispatch} = treeMind

  svg.on('mousedown.brush', function (event) {
    // 只允许鼠标左键进行 brush
    if (event.button !== 0) {
      return
    }
    start = true
    startX = event.offsetX
    startY = event.offsetY
  }).on('mousemove.brush', function (event) {
    permissionInterceptor(() => {
      if (start) {
        let endX = event.offsetX
        let endY = event.offsetY

        // 某些情况下鼠标微动但是坐标未变化，忽略这种情况
        if (Math.abs(startX - endX + startY - endY) <= 2) {
          return
        }

        if (brushFirst) {
          dispatch.emit(EVENT_TYPE_ENUM.BRUSH_START)
          brushFirst = false
        }

        const point = [
          [Math.min(startX, endX), Math.min(startY, endY)],
          [Math.max(startX, endX), Math.max(startY, endY)],
        ]

        svg.call(brushCall.move, point)
      }
    }, PERMISSION_HANDLER_ENUM.BRUSH)
  }).on('mouseup.brush', function () {
    if (start) {
      start = false
      brushFirst = true
      startX = 0
      startY = 0
      svg.call(brushCall.clear)
    }
  })
}
