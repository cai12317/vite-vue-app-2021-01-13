import {initZoomEvent} from "./zoomEvent.js";
import {initBrushEvent, initMouthEvent} from "./brushEvent.js";
import {select} from "d3";
import {TreeEvent} from "./treeDispatch.js";
import {ROOT_BOX_CLASS, WINDOW_EXTEND_LENGTH, TRANSLATE_MAX} from "../core/constants.js";
import {EVENT_TYPE_ENUM} from "./eventEnum.js";
import {PERMISSION_HANDLER_ENUM} from "../permission/permissionEnum.js";
import {boxContains} from "../utils.js";

/**
 *
 * @param treeMind {TreeMind}
 */
export function initEvent(treeMind) {

  let translateX = 0, translateY = 0, translateK = 1, realTranslateY = 0

  const {svg} = treeMind

  // zoom 事件
  const zoomCall = initZoomEvent(treeMind, function ({x, y, k}) {
    translateX = x
    realTranslateY = y
    translateY = treeMind.rectifyY(y, true)
    translateK = k
  })
  const rootBox = svg.select(ROOT_BOX_CLASS.selector)
  const brushCall = initBrushEvent(selection => {
    const [[x1, y1], [x2, y2]] = selection
    return treeMind.updateSelected([
      [(x1 - translateX) / translateK, (y1 - translateY) / translateK],
      [(x2 - translateX) / translateK, (y2 - translateY) / translateK]
    ])
  })

  // 选择器事件
  svg.call(brushCall)
  // 移除 brush 的默认事件
  svg.select('.overlay')
          .attr('pointer-events', 'none')
  // 自定义控制选区
  initMouthEvent(treeMind, brushCall)

  let startClick = false, timeStamp, focusTimeStamp = 0

  // 设置svg 的点击事件
  svg.on('mousedown.svg', function () {
    startClick = true
    timeStamp = Date.now()
  }).on('mouseup.svg', function (event) {

    focusTimeStamp = Date.now()
    treeMind.focus = true

    if (startClick && event.target.tagName === 'svg' && Date.now() - timeStamp < 200) {
      treeMind.selectedAll()
      treeMind.clearSearchStatus()
    }
    timeStamp = 0
    startClick = false
  })

  select(document).on('keyup.delete', function (event) {
    if (treeMind.focus && event.key === 'Delete') {
      treeMind.permissionInterceptor(() => {
        treeMind.dispatch.emit(EVENT_TYPE_ENUM.DELETE, new TreeEvent(event, treeMind.getSelected(), undefined))
      }, PERMISSION_HANDLER_ENUM.DELETE)
    }
  }).on('mouseup.focus', function () {
    if (Date.now() - focusTimeStamp > 50) {
      treeMind.focus = false
    }
    focusTimeStamp = 0
  })

  function nodeLocation(x, y) {

    // 判断当前Y轴的偏移值是否超过最大限制，TRANSLATE_MAX
    if (Math.abs(y) > TRANSLATE_MAX) {
      treeMind.translateOffsetY = Math.abs(y) - TRANSLATE_MAX

      y = treeMind.rectifyY(y)

    } else {
      treeMind.translateOffsetY = 0
    }

    x = x - translateX
    y = y - realTranslateY

    let zoomSvg = treeMind.svg
    if (Math.abs(x) < 2000 && Math.abs(y) < 2000) {
      zoomSvg = treeMind.svg.transition()
    }

    zoomSvg.call(zoomCall.translateBy, x, y)
  }

  function layoutCenter() {

    const {
      width: svgWidth,
      height: svgHeight
    } = treeMind.svg.node().getBoundingClientRect()

    const {
      width,
      height
    } = rootBox.node().getBoundingClientRect()

    translateX = (svgWidth - width) / 2
    translateY = (svgHeight - height) / 2

    treeMind.svg.transition()
            .call(zoomCall.translateBy, translateX, translateY)

  }

  return {
    zoomCall,
    nodeLocation,
    layoutCenter,
    inParentBox: function (x, y, node, beforeNode) {

      // 根据上一个已计算的节点对当前为计算节点 node ，的 area 在 x 轴上进行扩大 offsetX
      let extendWidth = 0

      if (beforeNode && node !== beforeNode && !node.layout) {
        extendWidth = beforeNode.style?.box?.offsetX || 0
      }

      const x1 = -WINDOW_EXTEND_LENGTH - extendWidth, y1 = -WINDOW_EXTEND_LENGTH,
              x2 = x + WINDOW_EXTEND_LENGTH - extendWidth, y2 = y + WINDOW_EXTEND_LENGTH
      const area = [
        [(x1 - translateX) / translateK, (y1 - translateY) / translateK],
        [(x2 - translateX) / translateK, (y2 - translateY) / translateK]
      ]
      return boxContains(area, node)
    }
  }
}
