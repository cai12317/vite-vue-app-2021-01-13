import {drag, select} from "d3";
import {setTheme, setBorderStrokeWidth} from "../theme/themeUtil.js";
import {TreeEvent} from "./treeDispatch.js";
import {NODE_BORDER_CLASS} from "../core/constants.js";
import {EVENT_TYPE_ENUM} from "./eventEnum.js";
import {PERMISSION_HANDLER_ENUM} from "../permission/permissionEnum.js";

/**
 *
 * @param treeMind {TreeMind}
 */
export function dragEvent(treeMind) {

  let startX,
          startY,
          offsetX = 0,
          offsetY = 0,
          dragStart = false,
          dropStart = false,
          mouseNode,
          mouseDomReset,
          dragNode = null

  function moveNode(node) {

    const res = treeMind.dispatch.emit(EVENT_TYPE_ENUM.DRAG_BEFORE, new TreeEvent(undefined, node, {
      dragBeforeParentNode: node.parent,
      dragAfterParentNode: mouseNode,
    }))
    // 判断是否需要移动
    const isFalse = res.some(v => v === false)

    if (!isFalse) {
      mouseDomReset && mouseDomReset()

      // 把拖拽节点移动到当前悬浮的节点
      const children = mouseNode.data.children || mouseNode.data._children || []
      children.push(node.data)

      // 把 node 从父节点移除
      const parentNode = node.parent
      const parentChildren = parentNode.data.children
      parentChildren.splice(node.order, 1)

      if (mouseNode.data._children) {
        mouseNode.data._children = null
      }

      mouseNode.data.children = children
      treeMind.update(mouseNode)

      treeMind.dispatch.emit(EVENT_TYPE_ENUM.DRAG_AFTER, new TreeEvent(undefined, node, {
        dragBeforeParentNode: parentNode,
        dragAfterParentNode: mouseNode,
      }))
    }
  }

  return {
    drag: drag()
            .filter(() => {
              return !treeMind.config.disabled
            })
            .on("start", function (event) {
              startX = event.x;
              startY = event.y;
            })
            .on('drag', function (event, node) {

              treeMind.permissionInterceptor(() => {

                offsetX += (event.x - startX)
                offsetY += (event.y - startY)

                startX = event.x
                startY = event.y

                if (Math.abs(offsetX + offsetY) <= 1) {
                  return
                }

                dragNode = node

                // 拖动开始需要先隐藏后代节点
                if (!dragStart) {
                  treeMind.visibleChildNode(node)
                  dragStart = true
                }

                const translate = node.translate || {x: 0, y: 0}
                const x = node.x + offsetX + translate.x
                const y = node.y + offsetY + translate.y

                select(this)
                        .attr('pointer-events', 'none')
                        .attr('transform', () => `translate(${x}, ${y})`)
                        .style('opacity', '0.5')

                treeMind.connection.updatePath({
                  source: node.parent,
                  target: {
                    data: node.data,
                    x,
                    y
                  }
                })

                treeMind.extraConnection.updatePath(node, x, y)
              }, PERMISSION_HANDLER_ENUM.DRAG)

            })
            .on('end', function (event, node) {
              if (dragStart) {
                select(this).attr('pointer-events', 'all')
                        .style('opacity', null)
                // 拖动结束 显示子节点
                treeMind.visibleChildNode(node, 'visible')

                if (dropStart && mouseNode) {
                  if (node.parent === mouseNode) {
                    treeMind.updatePosition(node)
                    mouseDomReset && mouseDomReset()
                  } else {
                    moveNode(node)
                  }

                } else {
                  treeMind.updatePosition(node, offsetX, offsetY)
                }

                dragStart = false
                offsetX = 0
                offsetY = 0
                dropStart = false
                mouseDomReset = null
              }
            }),
    mouseenter(event, node) {
      if (dragStart) {

        let nodeParent = node.parent

        while (nodeParent) {
          if (nodeParent === dragNode) {
            return
          } else {
            nodeParent = nodeParent.parent
          }
        }

        const mouseenterSelection = select(this).select(NODE_BORDER_CLASS.selector)
                .attr('stroke-width', setTheme('border-drop-stroke-width')(node))

        dropStart = true
        mouseNode = node

        mouseDomReset = () => {
          mouseenterSelection.attr('stroke-width', setBorderStrokeWidth()(node))
        }
      }
    },
    mouseleave() {
      if (dragStart) {
        mouseDomReset && mouseDomReset()
        mouseDomReset = null
        mouseNode = null
      }
    },
  }
}
