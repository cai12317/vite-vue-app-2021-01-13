export const EVENT_TYPE_ENUM = {
  // icon 点击事件
  'ICON_CLICK': 'iconClick',
  // 当 树 挂载完成后的事件
  'MOUNTED': 'mounted',
  // 拖拽完成后 准备把节点移动到新的节点前的事件, 监听器函数中，只要有一个函数的返回值为 false 就可以取消节点的移动
  'DRAG_BEFORE': 'dragBefore',
  // 拖拽完成后 节点完成移动后的事件
  'DRAG_AFTER': 'dragAfter',
  // 节点右键事件
  'CONTEXTMENU': 'contextmenu',
  // 节点选中的事件
  'SELECTED': 'selected',
  // 节点双击
  'NODE_DBLCLICK': 'nodeDblclick',
  // 点击删除
  'DELETE': 'delete',
  // 节点状态清除
  'CLEAR_SEARCH_STATUS': 'clearSearchStatus',
  'ZOOM_END': 'zoomEnd',
  'BRUSH_START': 'brushStart',
}
