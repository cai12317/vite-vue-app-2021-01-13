# TreeMind 基于 [D3](https://d3js.org/) 的可视化树图组件

大大增加了大数据下的渲染性能，在不考虑网络因素下，实现了：10W级数据毫秒级渲染，100W级数据秒级渲染，当数据量超过 10W
时，会使用 `requestIdleCallback` 进行性能优化，尽量保证操作不卡顿。

## 目录结构

``` lua
treeMind
├── treeMind.js -- 主文件入口，暴露了 TreeMind 类
├── utils.js -- 工具函数
├── init.js -- 初始化工具函数
├── core -- 核心源码
|    ├── baseConnection.js -- 连线的基础类
|    ├── baseNode.js -- 节点渲染模式的基础类，所有添加的新的节点渲染模式都需要继承它
|    ├── commonNode.js -- 简易节点渲染模式
|    ├── connection.js -- 节点连线类
|    ├── constants.js -- 定义了组件所包含的所有常量
|    ├── detailNode.js -- 详情节点渲染模式
|    ├── extraConnection.js -- 额外的连线类
|    ├── icon.js -- 创建 icon 的工具函数
|    ├── nodeElement.js -- 节点元素类，节点元素细节的排版和渲染
|    ├── nodeTypeEnum.js -- 节点类型枚举
|    ├── requestIdleCallbackUpdate.js -- 用于大数据下的数据处理优化，保证主线程不阻塞
├── event -- 事件源码
|    ├── brushEvent.js -- 自定义框选事件
|    ├── dragEvent.js -- 拖拽事件
|    ├── eventEnum.js -- treeMind 向外部暴露的事件枚举
|    ├── initEvent.js -- 事件初始化
|    ├── treeDispatch.js -- 自定义的发布订阅模式
|    ├── zoomEvent.js -- 内容的收索放大事件
├── example -- 例子代码
├── permission -- 权限控制
|    ├── permission -- 权限控制
|    ├── permissionEnum -- 权限类型枚举
```

## 使用

`TreeMind` 定义在 `src/components/treeMind/treeMind.js` 中

``` js
/**
 *
 * @param dom {Element} 渲染的外部容器
 * @param data 渲染的数据
 * @param config {{}|*} 配置
 * @param config.getName {Function} 获取节点文本信息
 * @param config.disabled {boolean} 是否禁用 true 为禁用 默认为 false
 * @param config.upTextKey {string} 线上文本的key 默认 upText
 * @param config.downTextKey {string} 线上文本的key 默认 downText
 * @param config.getKey {Function} 获取节点时的比对方法，若你想要在连线时只传入一个 key 就可以让 TreeMind 找到对应的 Node
 *                                  请为每一条数据指定唯一的 key 指，默认 d => d.id
 */
const treeMind = new TreeMind(dom, data, {
    disabled: false,
})


// data 是一个树结构的数据
const data = {
  "name": "flare",
  "text": "1564516456",
  "root": true,
  "id": 0,
  "children": [
    {
      "name": "analyticsanalyticsanalyticsanalyticsanalytics",
      "text": "62179967580111082821444412412412412543522",
      "heart": true,
      "punchClock": 3,
      "id": 1
    },
    {
      "name": "静安寺老壳大阿克苏老的辣",
      "text": "62179967580111082821444412412412412543522",
      "value": 3416,
      "id": 13
    }
  ]
}

```

## 事件

在实际操作中 `treeMind` 会派发出很多事件，具体都定义在 `src/components/treeMind/event/eventEnum.js` 中

## 额外的节点连线

需要使用 `treeMind.batchCreateExtraConnection` 进行创建，具体查看函数注释

## 自由的节点渲染模式

当前有两种节点渲染模式，都是依据以前的需求开发的，分别是：`commonNode` 和 `detailNode`
,每个节点的渲染内容主要依赖于内部的 `nodeRender`
函数，当前袁术渲染主要考虑的有 `文本`、`图片`、`文本背景`、`文本换行`、`自动排版` ，节点内的每一个元素都需要使用 `NodeElement`
进行创建，可以根据实际需要渲染的情况进去处理，详细配置如下

``` js
/**
 *
 * @param text {string}
 * @param option {{}}
 * @param option.maxWidth {number} 最大宽度，超过后会自动换行
 * @param option.tag {string}
 * @param option.background {string} 背景色
 * @param option.left {number}
 * @param option.listeners {[string, Function][]} 监听器
 * @param option.margin {{}}
 * @param option.margin.left {number}
 * @param option.margin.right {number}
 * @param option.margin.top {number}
 * @param option.margin.bottom {number}
 * @param option.attrs {{}} 额外的属性，会直接设置到真实节点上
 * @param option.fixed {{}} 是否使用固定定位
 * @param option.fixed.x {number} 是否使用固定定位
 * @param option.fixed.y {number} 是否使用固定定位
 * @param option.width {number} 图片属性
 * @param option.height {number} 图片属性
 */
const option = {
    attrs: {
        'font-weight': 'bold',
    },
}

const cardNameTitle = new NodeElement('卡号：', option)
```
