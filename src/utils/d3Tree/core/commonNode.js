import {tree} from "d3";
import {BaseNode} from "./baseNode.js";
import {NodeElement, ElementTypeEnum} from "./nodeElement.js";


export class CommonNode extends BaseNode {

  constructor(nodeBox, treeMind) {
    super(nodeBox, treeMind)
    this.defaultNodeWidth = 60
    this.defaultNodeHeight = 200
    this.defaultFontStyle['font-size'] = 16
    this.setHalfFontHeight()
    this.key = 'common-node'
  }

  treeLayout(root) {

    /** tree 创建一个新的有序的树型布局
     *  nodeSize 设置节点尺寸
     */
    const treeLayout = tree().nodeSize([this.defaultNodeWidth, this.defaultNodeHeight])

    return treeLayout(root)
  }

  nodeRender({data}) {

    const {
      name,
      heart,
      punchClock,
    } = data

    const children = []

    const cardNumber = new NodeElement(name)
    children.push(cardNumber)

    if (heart === true) {
      const heartEle = new NodeElement('/icons/heart.svg', {
        tag: ElementTypeEnum.IMAGE,
        width: 18,
        height: 18,
        margin: {
          top: -14
        }
      })
      children.push(heartEle)
    }

    if (punchClock > 0) {
      const heartEle = new NodeElement('/icons/punch-clock.svg', {
        tag: ElementTypeEnum.IMAGE,
        width: 18,
        height: 18,
        margin: {
          top: -14
        }
      })
      children.push(heartEle)
    }

    return [children]
  }
}
