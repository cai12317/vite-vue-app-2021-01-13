export class BaseConnection {
  /**
   * 水平连接线生成器
   */
  diagonal$
  /**
   * @var {[]}
   */
  links
  pathBox
  /**
   * @var {TreeMind}
   */
  treeMind
  gSelection

  constructor(pathBox, treeMind) {
    this.pathBox = pathBox
    this.treeMind = treeMind
    this.links = []
  }

  /** 获取普通连线的 节点定位信息 translate 表示节点的偏移量
   *
   * @param node
   * @param x
   * @param y
   */
  getDiagonalNode(node, x, y) {
    const translate = node.translate || {x: 0, y: 0}

    y = y + translate.y
    y = this.treeMind.rectifyY(y)

    return {
      x: x + translate.x,
      y,
    }
  }
}
