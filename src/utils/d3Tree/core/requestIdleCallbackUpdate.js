import {OPEN_REQUEST_IDLE_CALLBACK_DATA_LENGTH} from "./constants.js";

export class RequestIdleCallbackUpdate {
  // 更新队列
  update
  /**
   * @var {number} 数据分片长度
   */
  fragmentLength
  /**
   * @var {[]}
   */
  data
  /**
   * @var {Function}
   */
  eachCallback
  /**
   * @var {Function}
   */
  updateCallback
  /**
   * @var {Function}
   */
  eachAfterCallback
  /**
   * @var {number} 分片开始下标
   */
  fragmentStartIndex = 0
  cancelId

  /**
   * @param fragmentLength
   */
  constructor(fragmentLength = 20000) {
    this.fragmentLength = fragmentLength
  }

  /**
   *
   * @param data {[]}
   * @param eachCallback {Function}
   * @param updateCallback {Function}
   * @param eachAfterCallback {Function}
   */
  add(data, eachCallback, updateCallback, eachAfterCallback = null) {

    console.log(`数据量--${data.length}`)

    if (this.cancelId) {
      window.cancelIdleCallback(this.cancelId)
      this.cancelId = null
      this.fragmentStartIndex = 0
      // this.updateCallback()
      console.log('移除前置更新，并使用前一个更新回调更新一次---')
    }

    this.data = data
    this.eachCallback = eachCallback
    this.updateCallback = updateCallback
    this.eachAfterCallback = eachAfterCallback

    this.open()
  }

  open() {
    this.cancelId = window.requestIdleCallback(() => {
      this.cancelId = null
      if (this.fragmentStartIndex > this.data.length) {
        this.updateCallback()

        console.log('分片更新完成---')

        this.data = []
        this.eachCallback = null
        this.updateCallback = null
        this.fragmentStartIndex = 0
      } else {

        const data = this.data.slice(this.fragmentStartIndex, this.fragmentStartIndex + this.fragmentLength)
        for (const datum of data) {
          this.eachCallback(datum)
        }

        if (this.eachAfterCallback) {
          this.eachAfterCallback()
        }


        this.fragmentStartIndex += this.fragmentLength
        this.open()
      }

    }, {
      timeout: 0
    })
  }

  // 取消任务
  cancel() {
    if (this.cancelId) {
      window.cancelIdleCallback(this.cancelId)
      this.cancelId = null
      this.fragmentStartIndex = 0
      console.log('取消任务---')
    }
  }

  /**
   *
   * @param data {[]}
   * @param eachCallback {Function}
   * @param updateCallback {Function}
   * @param eachAfterCallback {Function}
   */
  useFragmentUpdate(data, eachCallback, updateCallback, eachAfterCallback = null) {
    if (data.length > OPEN_REQUEST_IDLE_CALLBACK_DATA_LENGTH) {
      this.add(data, eachCallback, updateCallback, eachAfterCallback)
    } else {
      data.forEach(eachCallback)
      updateCallback()
    }

  }
}
