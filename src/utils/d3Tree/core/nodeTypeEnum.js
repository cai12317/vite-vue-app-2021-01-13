import {CommonNode} from "./commonNode.js";
import {DetailNode} from "./detailNode.js";


export const NODE_TYPE_ENUM = {
  'COMMON_NODE': CommonNode,
  'DETAIL_NODE': DetailNode,
}
