// 容器的最小宽度
export const BOX_MIX_WIDTH = 600
// 容器的最小高度
export const BOX_MIX_HEIGHT = 400

export const DEFAULT_FONT_SIZE = 12

// 节点容器 class
export const NODE_CLASS = {
  cls: 'node-box',
  selector: '.node-box'
}
// 连线 class
export const PATH_CLASS = {
  cls: 'path-box',
  selector: '.path-box'
}
// 节点边框 class
export const NODE_BORDER_CLASS = {
  cls: 'node-border',
  selector: '.node-border'
}

// 收缩  展开 icon
export const USE_SWITCH_ICON_CLASS = {
  cls: 'use-switch-icon',
  selector: '.use-switch-icon',
}

// 根节点 class
export const ROOT_BOX_CLASS = {
  cls: 'root-box',
  selector: '.root-box',
}

// connection 外部容器
export const G_PATH_CLASS = {
  cls: 'g-path',
  selector: '.g-path',
}

// 线上文本
export const UP_TEXT = {
  cls: 'up-text',
  selector: '.up-text',
}

// 线下文本
export const DOWN_TEXT = {
  cls: 'down-text',
  selector: '.down-text',
}

// 额外连线 外部容器
export const G_EXTRA_PATH_CLASS = {
  cls: 'g-extra-path',
  selector: '.g-extra-path',
}

// 节点内容容器
export const NODE_CONTAINER = {
  cls: 'node-container',
  selector: '.node-container',
}

export const CONNECTION_ARROW_MARKER = 'connection-arrow-marker'

// 计算节点是否显示时，计算窗口额外扩展的宽度
// 扩展宽度增加会增加数据更新量
export const WINDOW_EXTEND_LENGTH = 1000

// 当数据量大于此值，数据检索：search、 updateNodeDisplay 会使用 requestIdleCallback 进行更新
export const OPEN_REQUEST_IDLE_CALLBACK_DATA_LENGTH = 100000

// 当 svg dom 的偏移值大于 +-33554448，节点渲染会出现问题，
// 经测试当总节点数量大于50W 最上面的节点渲染异常，
// 此值为节点数20W时最上面节点的偏移值
export const TRANSLATE_MAX = 20000000
