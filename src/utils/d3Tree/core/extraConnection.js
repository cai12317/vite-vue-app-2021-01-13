import {getExtraDiagonalNode, getLinkKey} from "../utils.js";
import {BaseConnection} from "./baseConnection.js";
import {linkVertical} from "d3";
import {G_EXTRA_PATH_CLASS, PATH_CLASS, CONNECTION_ARROW_MARKER} from "./constants.js";

const DEFAULT_EXTRA_CONNECTION_STROKE = '#408ec0'
const DEFAULT_EXTRA_CONNECTION_STROKE_WIDTH = '2'

function perfectStyle(style = {}) {
  const {
    stroke = DEFAULT_EXTRA_CONNECTION_STROKE,
    strokeWidth = DEFAULT_EXTRA_CONNECTION_STROKE_WIDTH
  } = style

  return {
    stroke,
    strokeWidth
  }
}

export class ExtraConnection extends BaseConnection {

  /**
   * @var {[]} 隐藏的连线
   */
  hideLinks

  constructor(pathBox, treeMind) {
    super(pathBox, treeMind)

    /** linkHorizontal 创建一个新的水平连接线生成器
     *  x 设置点的 x 存取器
     *  y 设置点的 y 存取器
     */
    this.diagonal$ = linkVertical().source(({source: {x, y}}) => {
      return {
        x: x,
        y: y + 8
      }
    }).x(d => d.x).y(d => d.y - 8)
    this.hideLinks = []
  }

  initLinks(nodes) {

    if (this.links.length + this.hideLinks.length === 0) {
      return
    }

    const nodeMap = new Map()
    const newLinks = []
    const links = this.links.concat(this.hideLinks)
    const hideLinks = []

    for (const node of nodes) {
      nodeMap.set(node.data._key, node)
    }

    for (const {source, target, style} of links) {
      const sourceKey = source.data._key
      const targetKey = target.data._key

      const newLink = {
        source: nodeMap.get(sourceKey) || source,
        target: nodeMap.get(targetKey) || target,
        style
      }

      if (nodeMap.has(sourceKey) && nodeMap.has(targetKey)) {
        newLinks.push(newLink)
      } else {
        hideLinks.push(newLink)
      }
    }

    this.links = newLinks
    this.hideLinks = hideLinks
  }

  update() {
    const visibleLinks = this.links.filter(({
                                              target,
                                              source: _source
                                            }) => target.display !== false || _source.display !== false)

    const gUpdateSelection = this.pathBox.selectAll(G_EXTRA_PATH_CLASS.selector)
            .data(visibleLinks, getLinkKey)

    // 更新连线选集
    gUpdateSelection.selectAll(PATH_CLASS.selector)
            .data(visibleLinks, getLinkKey)
            .transition()
            .attr('d', value => this.diagonal(value))

    const gEnterSelection = gUpdateSelection.enter()
            .append('g')
            .classed(G_EXTRA_PATH_CLASS.cls, true)

    // 添加连线
    gEnterSelection.append('path')
            .classed(PATH_CLASS.cls, true)
            .attr('stroke', d => d.style.stroke)
            .attr('stroke-width', d => d.style.strokeWidth)
            .attr('fill', 'none')
            .attr('marker-end', `url(#${CONNECTION_ARROW_MARKER})`)
            .attr('id', this.setPathId.bind(this))
            .attr('d', value => this.diagonal(value))

    this.gSelection = gUpdateSelection.merge(gEnterSelection)

    gUpdateSelection.exit().remove()
  }

  visiblePath() {
    this.gSelection.style('visibility', ({target, source}) => {
      if (target.visibility === 'hidden' || source.visibility === 'hidden') {
        return 'hidden'
      }

      return 'visible'
    })
  }

  updatePath(node, x, y) {
    let {gSelection} = this

    if (node) {
      gSelection = gSelection.filter(val => val.target === node || val.source === node)
      gSelection.selectAll(PATH_CLASS.selector)
              .attr('d', ({source, target}) => {

                const n = {
                  x,
                  y,
                  style: node.style,
                }

                if (source === node) {
                  source = n
                } else if (target === node) {
                  target = n
                }

                return this.diagonal({source, target})
              })
    } else {
      gSelection.selectAll(PATH_CLASS.selector)
              .transition()
              .attr('d', value => this.diagonal(value))
    }

  }

  setPathId(d) {
    return `extra-path-${this.treeMind.treeMindId}-${d.target.data._key}`
  }

  getPathId(d) {
    return `#${this.setPathId(d)}`
  }


  diagonal({source, target}) {
    return this.diagonal$(getExtraDiagonalNode(source, target))
  }

  createConnection(source, target, style) {
    this.links.push({
      source,
      target,
      style: perfectStyle(style)
    })
  }
}
