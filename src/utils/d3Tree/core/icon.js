import {theme} from "../theme/theme.js";
import {CONNECTION_ARROW_MARKER} from "./constants.js";

/** 添加一个 加/减 号符号 defs
 *
 * @param parent
 * @param add {boolean}  true 加号符 false 减号符号
 */
export function appendSwitchIconDef(parent, add = true) {

  const id = add ? 'add-icon' : 'close-icon'
  const d = add ? 'M 3,6 L 9,6 M 6,3 L 6,9 z' : 'M 3,6 L 9,6 z'

  const g = parent.select('defs')
          .append('g')
          .attr('id', id)
          .style('cursor', 'pointer')

  g.append('circle')
          .attr('cx', 6)
          .attr('cy', 6)
          .attr('r', 5)
          .attr('stroke-width', theme['switch-icon-stroke-width'])
          .attr('stroke', theme['switch-icon-stroke'])
          .attr('fill', '#FFFFFF')

  g.append('path')
          .attr('d', d)
          .attr('stroke-width', theme['switch-icon-stroke-width'])
          .attr('stroke', theme['switch-icon-stroke'])
}

export function appendArrowIconDef(parent) {
  const path = 'M 0 9 L 0 0 L 9 4 Z'

  const marker = parent.select('defs')
          .append('marker')
          .attr('id', CONNECTION_ARROW_MARKER)
          .attr('viewBox', '0 0 10 10')
          .attr('refX', '1')
          .attr('refY', '4')
          .attr('markerUnits', 'userSpaceOnUse')
          .attr('markerWidth', 10)
          .attr('markerHeight', 10)
          .attr('orient', 'auto')


  marker.append('path')
          .attr('d', path)
          .attr('stroke-width', theme['switch-icon-stroke-width'])
          .attr('stroke', 'transparent')
          .attr('fill', theme['switch-icon-stroke'])
}
