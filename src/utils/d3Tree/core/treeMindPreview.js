import {select, linkHorizontal, drag} from "d3";
import {NODE_CLASS, NODE_BORDER_CLASS, PATH_CLASS} from "./constants.js";
import {createBoxPath, getLinkKey, getNodeTranslate} from "../utils.js";
import {setTheme} from "../theme/themeUtil.js";
import {theme} from "../theme/theme.js";
import {EVENT_TYPE_ENUM} from "../event/eventEnum.js";

const offset = 200

export class TreeMindPreview {

  /**
   * @var {TreeMind}
   */
  treeMind
  /**
   * 预览容器 svg
   */
  container
  /**
   * 连线容器
   */
  pathContainer
  /**
   * 节点容器
   */
  nodeContainer
  /**
   * 视口矩形
   */
  viewBox
  diagonal$
  drag
  minY = 0
  maxY = 0
  maxX = 0
  offsetX = 0
  offsetY = 0
  scale = 1
  dragStart = false

  /**
   * @param treeMind {TreeMind}
   */
  constructor(treeMind) {
    this.treeMind = treeMind
    const {dom} = treeMind

    this.diagonal$ = linkHorizontal().x(d => d.x).y(d => d.y)

    this.createPath = this.createPath.bind(this)

    this.container = select(dom).append('svg')
            .attr('width', 260)
            .attr('height', 200)
            .style('position', 'absolute')
            .style('bottom', '10px')
            .style('left', '10px')
            .style('box-shadow', '0 0 3px 1px #ccc')
            .style('background', '#fff')
    this.pathContainer = this.container.append('g')
    this.nodeContainer = this.container.append('g')

    let startX, startY

    this.viewBox = this.container.append('rect')
            .attr('x', 0)
            .attr('y', 0)
            .attr('stroke', 'red')
            .attr('stroke-width', '5')
            .attr('fill', 'transparent')
            .attr('cursor', 'pointer')
            .call(drag()
                    .on("start", function (event) {
                      startX = event.x;
                      startY = event.y;
                    })
                    .on('drag', event => {
                      this.dragStart = true

                      let currentOffsetX = event.x - startX
                      let currentOffsetY = event.y - startY

                      startX = event.x
                      startY = event.y

                      const translate = this.viewBoxIsOut(this.offsetX, this.offsetY, currentOffsetX, currentOffsetY)

                      currentOffsetX = translate.currentOffsetX
                      currentOffsetY = translate.currentOffsetY

                      this.offsetX += currentOffsetX
                      this.offsetY += currentOffsetY

                      const {svg, layoutUtil} = treeMind
                      svg.call(layoutUtil.zoomCall.translateBy, -(currentOffsetX), -(currentOffsetY))

                      this.updateViewBox()
                    })
                    .on('end', () => {
                      this.dragStart = false
                    }))

    this.initListeners()
  }

  initListeners() {
    this.treeMind.dispatch.on(EVENT_TYPE_ENUM.ZOOM_END, (zoomEvent) => {

      if (this.dragStart) {
        return
      }

      const {transform: {x, y, k}} = zoomEvent

      // 按照反向变换 viewBox 包括 scale 都需要放到 translate 之前
      this.offsetX = -x
      this.offsetY = -y
      this.scale = 1 / (k || 1)
      this.updateViewBox()
    })
  }

  viewBoxIsOut(offsetX, offsetY, currentOffsetX, currentOffsetY) {
    const {boundingClientRect} = this.treeMind

    // 左 右
    if (currentOffsetX < 0 && offsetX < -offset || currentOffsetX > 0 && (offsetX + boundingClientRect.width) > (this.maxX + offset * 6)) {
      currentOffsetX = 0
    }

    // 上 下
    if (currentOffsetY < 0 && offsetY < (this.minY - offset) || currentOffsetY > 0 && (offsetY + boundingClientRect.height) > (this.maxY + offset * 2)) {
      currentOffsetY = 0
    }

    return {currentOffsetX, currentOffsetY}
  }

  updateBox() {
    this.container
            .attr('viewBox', `0 ${this.minY - offset} ${this.maxX + offset * 2} ${this.maxY - this.minY + offset * 2}`)

    const {boundingClientRect} = this.treeMind

    this.viewBox.attr('width', boundingClientRect.width)
            .attr('height', boundingClientRect.height)
  }

  updateViewBox() {
    this.viewBox.attr('transform', `scale(${this.scale}) translate(${this.offsetX},${this.offsetY})`)
  }

  updateSize(node) {
    this.minY = Math.min(this.minY, node.y)
    this.maxX = Math.max(this.maxX, node.x)
    this.maxY = Math.max(this.maxY, node.y)
  }

  update() {
    this.updateNode(this.treeMind.baseNode.nodes)
    this.updateConnection(this.treeMind.connection.links)
    this.updateBox()
  }

  updateNode(nodes) {
    const selection = this.nodeContainer.selectAll(NODE_CLASS.selector)
            .data(nodes, v => {
              this.updateSize(v)
              return v.data._key
            })

    selection.attr('transform', getNodeTranslate)

    selection.selectAll(NODE_BORDER_CLASS.selector)
            .data(d => [d], v => v.data._key)
            .attr('d', this.createPath)
            .attr('transform', d => `translate(0, ${this.getBoxOffsetY(d)})`)

    selection.enter()
            .append('g')
            .classed(NODE_CLASS.cls, true)
            .attr('transform', getNodeTranslate)
            .classed(NODE_CLASS.cls, true)
            .append('path')
            .classed(NODE_BORDER_CLASS.cls, true)
            .attr('d', this.createPath)
            .attr('fill', () => setTheme('fill')({depth: 0}))
            .attr('transform', d => `translate(0, ${this.getBoxOffsetY(d)})`)

    selection.exit().remove()
  }

  updateConnection(links) {
    const selection = this.pathContainer.selectAll(PATH_CLASS.selector)
            .data(links, getLinkKey)
    selection.attr('d', value => this.diagonal(value))

    selection.enter()
            .append('path')
            .classed(PATH_CLASS.cls, true)
            .attr('stroke', theme['connection-stroke'])
            .attr('stroke-width', 5)
            .attr('fill', 'none')
            .attr('d', value => this.diagonal(value))

    selection.exit().remove()
  }

  createPath(d) {
    const {defaultNodeWidth, defaultNodeHeight} = this.treeMind.baseNode
    const height = d?.style?.box?.height || defaultNodeHeight
    const width = d?.style?.box?.width || defaultNodeWidth

    return createBoxPath(height, width)
  }

  getBoxOffsetY(d) {
    const {defaultNodeHeight} = this.treeMind.baseNode
    const offsetY = d?.style?.box?.offsetY
    if (offsetY) {
      return offsetY
    } else {
      return defaultNodeHeight / 2
    }
  }

  getDiagonalNode(node, x, y) {
    const translate = node.translate || {x: 0, y: 0}
    return {
      x: x + translate.x,
      y: y + translate.y,
    }
  }

  diagonal({source, target}) {
    const sourceWidth = source.style?.box?.width || 0
    return this.diagonal$({
      source: this.getDiagonalNode(source, source.x + sourceWidth, source.y),
      target: this.getDiagonalNode(target, target.x, target.y),
    })
  }
}
