import {getBBox} from "../utils.js";
import {DEFAULT_FONT_SIZE} from "./constants.js";

export const ElementTypeEnum = {
  'TEXT': 'text',
  'IMAGE': 'image',
  'RECT': 'rect',
}

export class NodeElement {

  /** 超出最大长度自动换行
   * @var {number}
   */
  maxWidth

  /** 内容 tag 为 image 时 text 为 href
   * @var {string}
   */
  text
  /** 元素额外属性
   * @var {{}}
   */
  attrs
  /** 监听器
   * @var {[string, Function][]}
   */
  listeners

  /** 外边距
   @var {{}}
   */
  margin

  /** 距离左边的固定距离
   *  设置了固定距离，后面的元素起始位置会从这个元素的初始位置开始
   @var {number}
   */
  left

  /** 背景色
   * @var {string}
   */
  background

  /** 背景元素
   * @var {ElementItem}
   */
  _backgroundEle

  /**
   * @var {ElementItem[]}
   */
  elements
  /**
   * @var {string} 标签类型
   */
  tag
  /**
   * @var {number} 只有 image 才会用到
   */
  width
  /**
   * @var {number} 只有 image 才会用到
   */
  height
  /** 是否使用固定定位
   * @var {{}}
   */
  fixed

  /**
   *
   * @param text {string}
   * @param option {{}}
   * @param option.maxWidth {number} 最大宽度，超过后会自动换行
   * @param option.tag {string}
   * @param option.background {string} 背景色
   * @param option.left {number}
   * @param option.listeners {[string, Function][]} 监听器
   * @param option.margin {{}}
   * @param option.margin.left {number}
   * @param option.margin.right {number}
   * @param option.margin.top {number}
   * @param option.margin.bottom {number}
   * @param option.attrs {{}} 额外的属性，会直接设置到真实节点上
   * @param option.fixed {{}} 是否使用固定定位
   * @param option.fixed.x {number} 是否使用固定定位
   * @param option.fixed.y {number} 是否使用固定定位
   * @param option.width {number} 图片属性
   * @param option.height {number} 图片属性
   */
  constructor(text, option = {}) {

    const {
      maxWidth,
      tag = ElementTypeEnum.TEXT,
      background,
      left,
      listeners = [],
      margin = {},
      attrs = {},
      fixed,
      width,
      height,
    } = option

    this.text = text
    this.maxWidth = maxWidth
    this.elements = []
    this.tag = tag
    this.background = background
    this.left = left
    this.listeners = listeners
    this.margin = {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      ...margin,
    }
    this.attrs = attrs
    this.fixed = fixed
    this.width = width
    this.height = height
  }

  render(...args) {

    switch (this.tag) {
      case ElementTypeEnum.TEXT:
        this.textRender(...args)
        break
      case ElementTypeEnum.IMAGE:
        this.imageRender()
    }

  }

  imageRender() {
    const ele = new ElementItem(ElementTypeEnum.IMAGE, {
      text: this.text,
      width: this.width,
      height: this.height,
      ...this.fixed
    })
    this.elements.push(ele)
  }

  textRender(parentSvg, defaultFontStyle) {

    this.attrs = {
      ...defaultFontStyle,
      ...this.attrs,
    }

    const {height, width} = this.getElementBBox(parentSvg, this.text)

    if (!this.maxWidth || (this.maxWidth && width <= this.maxWidth)) {

      this.elements.push(new ElementItem(ElementTypeEnum.TEXT, {
        text: this.text,
        width,
        height,
        ...this.attrs
      }))

    } else {

      const {length} = this.text

      let startIndex = 0
      let endIndex = 1
      let beforeWidth, beforeHeight

      while (true) {
        const text = this.text.substring(startIndex, endIndex)
        const {height, width} = this.getElementBBox(parentSvg, text)

        if (width > this.maxWidth || endIndex === length) {

          let end = endIndex

          if (endIndex - 1 === startIndex || width < this.maxWidth && endIndex === length) {
            // 至少保证每次取一个字符
            end = endIndex
            beforeWidth = width
            beforeHeight = height
          } else {
            end = endIndex - 1
          }


          this.elements.push(new ElementItem(ElementTypeEnum.TEXT, {
            text: this.text.substring(startIndex, end),
            width: width > this.maxWidth ? beforeWidth : width,
            height: this.maxWidth ? beforeHeight : height,
            ...this.attrs
          }))

          startIndex = end
          endIndex = startIndex + 1


          if (endIndex > length) {
            break
          }

        } else {
          beforeWidth = width
          beforeHeight = height
          endIndex++
        }
      }
    }

    if (this.background) {
      this._backgroundEle = new ElementItem(ElementTypeEnum.RECT)
      this._backgroundEle.setAttr('fill', this.background)
    }

  }

  getElementBBox(parentSvg, text) {
    const fontSize = this.attrs['font-size'] || DEFAULT_FONT_SIZE
    return getBBox(parentSvg, text, fontSize)
  }
}


class ElementItem {
  /**
   @var {string} 元素类型 text rect
   */
  type
  /**
   @var {{}}
   */
  attr

  /**
   *
   * @param type
   * @param attr {{}}
   * @param attr.width {number}
   * @param attr.height {number}
   * @param attr.x {number}
   * @param attr.y {number}
   * @param attr.text {string} type 为 image 时 text 为 href
   */
  constructor(type, attr = {}) {
    this.type = type
    this.attr = attr

    if (type === ElementTypeEnum.IMAGE) {
      this.attr['xlink:href'] = attr.text
    }

  }

  append(selection) {

    const {
      type,
      attr,
    } = this

    const thisSelection = selection.append(type)

    if (type === ElementTypeEnum.TEXT) {
      thisSelection.text(attr.text)
    }

    for (const key in attr) {
      thisSelection.attr(key, attr[key])
    }

    return thisSelection
  }

  setAttr(key, value) {
    this.attr[key] = value
  }
}
