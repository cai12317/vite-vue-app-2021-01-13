import {BaseNode} from "./baseNode.js";
import {tree} from "d3";
import {NodeElement, ElementTypeEnum} from "./nodeElement.js";

export class DetailNode extends BaseNode {

  constructor(nodeBox, treeMind) {
    super(nodeBox, treeMind)
    this.defaultNodeWidth = 200
    this.defaultNodeHeight = 260
    this.setHalfFontHeight()
    this.key = 'detail-node'
  }

  treeLayout(root) {

    /** tree 创建一个新的有序的树型布局
     *  nodeSize 设置节点尺寸
     */
    const treeLayout = tree().nodeSize([this.defaultNodeWidth, this.defaultNodeHeight])

    return treeLayout(root)
  }

  nodeRender({data}) {
    const {
      text = '62179967580111082821444412412412412543522',
      level = 3,
      name = '张三',
      a = '网银转账',
      b = '银行卡',
      c = '中国邮政储蓄银行股份有限公司',
      d = '返还(0) 止付(0) 冻结(0)',
      remark = '联系手机：17608278745 住宅地址：四川省平昌县青云镇龙寨村1组12号',
      accessory = 3,
      dataType = 0,
      stopPay = true,
      image = '/vue3.png'
    } = data || {}

    const boldFont = {
      attrs: {
        'font-weight': 'bold'
      }
    }

    const cardNameTitle = new NodeElement('卡号：', boldFont)

    const cardNum = new NodeElement(`${text}(${level}级卡)`, {maxWidth: 280})

    // 诈骗支付场景
    const fraudulentPaymentScenarioTitle = new NodeElement('诈骗支付场景：', boldFont)
    const fraudulentPaymentScenario = new NodeElement(a)

    // 号码来源
    const sourceNumberTitle = new NodeElement('号码来源：', boldFont)
    const sourceNumber = new NodeElement(b)

    // 号码归属
    const numberAttributionTitle = new NodeElement('号码来源：', boldFont)
    const numberAttribution = new NodeElement(c)

    // 数据类型
    const dataTypeEle = new NodeElement(dataType === 0 ? '资金流' : '信息流', {background: '#D2E1EB'})
    // 是否支持止付
    const isStopPay = new NodeElement(stopPay ? '支持止付' : '不支持止付', {
      background: '#D2E1EB',
      margin: {
        right: 2
      }
    })
    // 附件
    const accessoryEle = new NodeElement(`附件(${accessory})`, {
      left: 180,
      attrs: {
        fill: 'rgb(45, 140, 240)',
        cursor: 'pointer'
      },
      listeners: [
        [
          'click',
          function () {
            console.log(arguments)
          }
        ]
      ]

    })
    // 姓名
    const nameEleTitle = new NodeElement('嫌疑人:', {
      margin: {left: 8},
      ...boldFont
    })
    const nameEle = new NodeElement(name)

    // dEle
    const dEle = new NodeElement(d)

    // remarkEle
    const remarkTitle = new NodeElement('备注：', boldFont)
    const remarkEle = new NodeElement(remark)
    const imageEle = new NodeElement(image, {
      tag: ElementTypeEnum.IMAGE,
      fixed: {
        x: 340,
        y: -12,
      },
      width: 76,
      height: 80,
    })

    /** 二位数组的第一维度表示 渲染的行
     *  第二维度表示 每一行内需要渲染的内容，
     *  NodeElement.elements 数组长度大于 1 时，表示字符串需要换行
     */
    return [
      [cardNameTitle, cardNum],
      [fraudulentPaymentScenarioTitle, fraudulentPaymentScenario],
      [sourceNumberTitle, sourceNumber],
      [numberAttributionTitle, numberAttribution],
      [dataTypeEle, isStopPay, accessoryEle, nameEleTitle, nameEle],
      [dEle],
      [remarkTitle, remarkEle],
      [imageEle],
    ]
  }
}
