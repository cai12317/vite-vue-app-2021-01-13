import {getNodeRect, getLinkKey} from "../utils.js";
import {linkHorizontal} from "d3";
import {theme} from "../theme/theme.js";
import {BaseConnection} from "./baseConnection.js";
import {G_PATH_CLASS, PATH_CLASS, UP_TEXT, DOWN_TEXT, CONNECTION_ARROW_MARKER} from "./constants.js";

export class Connection extends BaseConnection {

  /**
   * @var {string}
   */
  upTextKey
  /**
   * @var {string}
   */
  downTextKey
  /** 是否显示连线上的文本
   * @var {boolean}
   */
  connectionTextVisible

  constructor(pathBox, treeMind) {
    super(pathBox, treeMind)
    this.upTextKey = treeMind.config.upTextKey
    this.downTextKey = treeMind.config.downTextKey
    this.connectionTextVisible = false

    /** linkHorizontal 创建一个新的水平连接线生成器
     *  x 设置点的 x 存取器
     *  y 设置点的 y 存取器
     */
    this.diagonal$ = linkHorizontal().source(({source: {x, y}}) => {
      return {
        x: x + 8,
        y: y
      }
    }).x(d => d.x - 8).y(d => d.y)
  }

  initLinks(root) {
    this.links = root.links()
  }

  /**
   * @param source 更新起始节点
   * @param updateLinks {[]} 更新节点
   */
  update({source, updateLinks}) {

    updateLinks = updateLinks || this.links
    console.log('links-更新量---', updateLinks.length)

    const gUpdateSelection = this.pathBox.selectAll(G_PATH_CLASS.selector)
            .data(updateLinks, getLinkKey)

    // 更新连线选集
    gUpdateSelection.selectAll(PATH_CLASS.selector)
            .data(updateLinks, getLinkKey)
            .transition()
            .attr('d', value => this.diagonal(value))

    const gEnterSelection = gUpdateSelection.enter()
            .append('g')
            .classed(G_PATH_CLASS.cls, true)
            .style('opacity', 0)

    // 添加连线
    const pathEnterSelection = gEnterSelection.append('path')
            .classed(PATH_CLASS.cls, true)
            .attr('stroke', theme['connection-stroke'])
            .attr('stroke-width', theme['connection-stroke-width'])
            .attr('fill', 'none')
            .attr('marker-end', `url(#${CONNECTION_ARROW_MARKER})`)
            .attr('id', this.setPathId.bind(this))
            .attr('d', ({target, source: _source}) => this.diagonal({
              target: source || target,
              source: source || _source
            }))

    this.gSelection = gUpdateSelection.merge(gEnterSelection)
    this.gSelection.transition()
            .style('opacity', 1)

    pathEnterSelection.transition()
            .attr('d', value => this.diagonal(value))

    gUpdateSelection.exit().remove()
  }

  diagonal({source, target}) {
    const sourceWidth = source.style?.box?.width || 0
    return this.diagonal$({
      source: this.getDiagonalNode(source, source.x + sourceWidth, source.y),
      target: this.getDiagonalNode(target, target.x, target.y),
    })
  }

  updatePath(v) {
    let {gSelection} = this

    if (v) {
      gSelection = gSelection.filter(val => val.target.data._key === v.target.data._key)
      gSelection.selectAll(PATH_CLASS.selector)
              .attr('d', this.diagonal(v))

      if (this.connectionTextVisible) {
        gSelection.selectAll(UP_TEXT.selector)
                .attr('transform', updateTextUpTransform(v))
        gSelection.selectAll(DOWN_TEXT.selector)
                .attr('transform', updateTextDownTransform(v))
      }

    } else {
      gSelection.selectAll(PATH_CLASS.selector)
              .transition()
              .attr('d', value => this.diagonal(value))

      if (this.connectionTextVisible) {
        gSelection.selectAll(UP_TEXT.selector)
                .attr('transform', updateTextUpTransform)
        gSelection.selectAll(DOWN_TEXT.selector)
                .attr('transform', updateTextDownTransform)
      }
    }
  }

  visiblePath() {
    this.gSelection.style('visibility', d => d.target.visibility)
  }

  setPathId(d) {
    return `path-${this.treeMind.treeMindId}-${d.target.data._key}`
  }

  getPathId(d) {
    return `#${this.setPathId(d)}`
  }

  appendConnectionText() {
    // 添加文本
    this.gSelection.append('text')
            .attr('transform', updateTextUpTransform)
            .attr('fill', 'red')
            .attr('text-anchor', 'middle')
            .classed(UP_TEXT.cls, true)
            .append('textPath')
            .attr('xlink:href', this.getPathId.bind(this))
            .attr('startOffset', '60%')
            .text(d => d.target.data[this.upTextKey])


    this.gSelection.append('text')
            .attr('transform', updateTextDownTransform)
            .attr('fill', 'red')
            .attr('text-anchor', 'middle')
            .classed(DOWN_TEXT.cls, true)
            .append('textPath')
            .attr('xlink:href', this.getPathId.bind(this))
            .attr('startOffset', '60%')
            .text(d => d.target.data[this.downTextKey])
  }

  hideConnectionText() {
    this.gSelection.selectAll(UP_TEXT.selector).remove()
    this.gSelection.selectAll(DOWN_TEXT.selector).remove()
  }

  visibleConnectionText() {
    if (this.connectionTextVisible) {
      this.hideConnectionText()
    } else {
      this.appendConnectionText()
    }
    this.connectionTextVisible = !this.connectionTextVisible
  }
}

function updateTextUpTransform({target, source}) {
  target = getNodeRect(target)
  source = getNodeRect(source)
  return `translate(${target.y < source.y ? -6 : 6} -6)`
}

function updateTextDownTransform({target, source}) {
  target = getNodeRect(target)
  source = getNodeRect(source)
  return `translate(${target.y < source.y ? 6 : -6} 18)`
}
