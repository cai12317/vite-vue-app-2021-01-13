import {dragEvent} from "../event/dragEvent.js";
import {createBoxPath, isRoot, getBBox, filterSwitchIcon, boxContains, getSelectionData} from "../utils.js";
import {setTheme, getDepthType, setBorderStrokeWidth} from "../theme/themeUtil.js";
import {select, create} from "d3";
import {TreeEvent} from "../event/treeDispatch.js";
import {theme} from "../theme/theme.js";
import {DEFAULT_FONT_SIZE, NODE_CLASS, NODE_BORDER_CLASS, USE_SWITCH_ICON_CLASS, NODE_CONTAINER} from "./constants.js";
import {EVENT_TYPE_ENUM} from "../event/eventEnum.js";
import {PERMISSION_HANDLER_ENUM} from "../permission/permissionEnum.js";

export class BaseNode {

  /** 每个继承了 baseNode 的 node对象，都需要在初始化时指定唯一的 key
   * @var {string}
   */
  key
  nodeBox
  /**
   * @var {[]} 当前的所有节点
   */
  nodes
  nodeSelection
  /**
   * @var {TreeMind}
   */
  treeMind
  drag
  /**
   * @var {number} 默认的节点宽度
   */
  defaultNodeWidth
  /**
   * @var {number} 默认的节点高度
   */
  defaultNodeHeight

  /**
   * @var {number} 文本行间距
   */
  textRowMargin
  /**
   * @var {number} 文本列间距
   */
  textColumnMargin
  /**
   * @var {number} 设置字体大小一半的高度
   */
  halfFontHeight
  /**
   * @var {{}} 默认的字体样式
   */
  defaultFontStyle
  /**
   * @var {[]} 当前选中的节点
   */
  selectedNodes
  /** key => node 的 map
   * @var {{}}
   */
  nodeMap = Object.create(null)

  constructor(nodeBox, treeMind) {
    this.nodeBox = nodeBox
    this.treeMind = treeMind
    this.textRowMargin = 4
    this.textColumnMargin = 4
    this.defaultFontStyle = {
      fill: '#000000',
      'stroke-width': '1',
      'font-size': DEFAULT_FONT_SIZE,
    }
    this.selectedNodes = []
    this.getNodeTranslate = this.getNodeTranslate.bind(this)

    // 初始化 拖拽事件
    this.drag = dragEvent(treeMind)

    treeMind.dispatch.on(EVENT_TYPE_ENUM.BRUSH_START, this.clearSelectedStatus.bind(this))
  }

  initNodes(root) {
    this.nodes = root.descendants()
  }

  /**
   * @param source 更新起始节点
   * @param updateNodes {[]} 更新节点
   * @param toggleMode {boolean} 更新是否切换了 节点模式
   */
  update({source, updateNodes, toggleMode = false} = {}) {

    updateNodes = updateNodes || this.nodes

    console.log('nodes-更新量---', updateNodes.length)

    const nodeUpdateSelection = this.nodeBox.selectAll(NODE_CLASS.selector)
            .data(updateNodes, v => v.data._key)

    nodeUpdateSelection
            .transition()
            .attr('transform', this.getNodeTranslate)

    // 创建 node 节点
    const nodeEnterSelection = nodeUpdateSelection.enter()
            .append('g')
            .classed(NODE_CLASS.cls, true)
            .attr('transform', d => {
              return source ? `translate(${source.x}, ${source.y})` : this.getNodeTranslate(d)
            })
            .style('opacity', 0)

    this.applyEvent(nodeEnterSelection)

    // 向每个节点 添加 用于切换子节点是否展开的icon 并更新数据
    this.updateSwitchIcon(nodeUpdateSelection, nodeEnterSelection)

    this.nodeSelection = nodeUpdateSelection.merge(nodeEnterSelection)

    // 向每个 node 节点中添加外边框
    this.updateBorder(this.nodeSelection)
    this.updateContent(nodeEnterSelection, nodeUpdateSelection, toggleMode)

    this.nodeSelection.transition()
            .style('opacity', 1)
            .attr('transform', this.getNodeTranslate)

    nodeUpdateSelection.exit().remove()
  }

  treeLayout() {
  }

  nodeRender() {
  }

  updateBorder(updateSelection) {

    updateSelection = updateSelection || this.nodeSelection

    updateSelection = updateSelection.selectAll(NODE_BORDER_CLASS.selector)
            .data(d => [d], v => v.data._key)

    updateSelection.attr('stroke-width', setBorderStrokeWidth())
            .attr('stroke', setTheme('border-color'))
            .transition()
            .attr('d', d => createBoxPath(d.style.box.height, d.style.box.width))
            .attr('transform', d => `translate(0, ${d.style.box.offsetY})`)

    updateSelection.enter()
            .append('path')
            .classed(NODE_BORDER_CLASS.cls, true)
            .attr('d', d => createBoxPath(d.style.box.height, d.style.box.width))
            .attr('fill', setTheme('fill'))
            .attr('stroke', setTheme('border-color'))
            .attr('stroke-width', setBorderStrokeWidth())
            .attr('transform', d => `translate(0, ${d.style.box.offsetY})`)
  }

  /** 每个节点的宽度可能和比初始定义的 defaultNodeWidth 大，
   *  这就需要把所有后代节点累计向后偏移多出的那一部分尺寸
   *
   * @param node
   */
  resetNodeX(node) {
    // 从新计算当前节点的 x 位置
    // 在原位置的基础上增加父级节点多出默认宽度的宽度
    if (node.parent) {
      let parentWidthOffset = 0
      if (node.parent.style.box.width > this.defaultNodeWidth) {
        parentWidthOffset = node.parent.style.box.width - this.defaultNodeWidth
      }

      node.style.box.offsetX += (node.parent.style.box.offsetX + parentWidthOffset)
    }

    node.x += node.style.box.offsetX
  }

  // 绑定新添加节点的事件
  applyEvent(enterSelection) {
    const that = this
    const {treeMind} = this

    let beforeClickNode, beforeClickTimestamp = 0

    enterSelection.on('click', function (event, node) {
      treeMind.permissionInterceptor(() => {

        const sameClickNode = beforeClickNode === node.data._key
        const timeDiff = Date.now() - beforeClickTimestamp
        // 快速点击默认设置为选中
        const isSelected = sameClickNode && timeDiff < 300
        console.log(isSelected, timeDiff)
        beforeClickNode = node.data._key
        beforeClickTimestamp = Date.now()

        let index = (node.selected || isSelected) ? that.selectedNodes.findIndex(v => v.data._key === node.data._key) : -1

        node.selected = !node.selected || isSelected

        select(this).select(NODE_BORDER_CLASS.selector)
                .attr('stroke-width', setBorderStrokeWidth()(node))
        if (index === -1) {
          that.selectedNodes.push(node)
        } else if (!isSelected) {
          that.selectedNodes.splice(index, 1)
        }

        treeMind.dispatchSelected(that.selectedNodes)
      }, PERMISSION_HANDLER_ENUM.NODE_CLICK)
    })
            .on('contextmenu', function (event, node) {
              treeMind.permissionInterceptor(() => {
                event.stopPropagation()
                event.preventDefault()
                treeMind.dispatch.emit(EVENT_TYPE_ENUM.CONTEXTMENU, new TreeEvent(event, node))
              }, PERMISSION_HANDLER_ENUM.NODE_CONTEXTMENU)
            })
            .on('dblclick', function (event, node) {
              treeMind.permissionInterceptor(() => {
                treeMind.dispatch.emit(EVENT_TYPE_ENUM.NODE_DBLCLICK, new TreeEvent(event, node))
              }, PERMISSION_HANDLER_ENUM.NODE_DBLCLICK)
            })
            .on('mouseenter', this.drag.mouseenter)
            .on('mouseleave', this.drag.mouseleave)
            .on('mouseup', this.drag.mouseup)

    // 除根节点外绑定拖拽事件
    enterSelection.filter(d => !isRoot(d))
            .call(this.drag.drag)
  }

  /**
   *
   * @param list {[NodeElement][]}
   */
  patchNode(list) {

    const {
      treeMind: {
        hiddenSvg
      },
      textRowMargin,
      textColumnMargin,
      halfFontHeight
    } = this

    let initialY = 0, width = 0, fixedMaxY = 0

    for (const item of list) {

      let initialX = 0

      let maxY = 0

      for (const nodeEle of item) {
        nodeEle.render(hiddenSvg, this.defaultFontStyle)

        if (nodeEle.fixed) {
          const {
            fixed: {
              y = 0,
              x = 0
            },
            height,
            width: w
          } = nodeEle

          fixedMaxY = Math.max(fixedMaxY, y + height)
          width = Math.max(width, x + w)

        } else {

          const {
            left,
            margin,
            elements,
            _backgroundEle,
          } = nodeEle

          if (left) {
            initialX = left
          }

          if (margin.left) {
            initialX += margin.left
          }

          let maxX = 0, afterY = initialY, sumHeight = 0
          for (const {attr} of elements) {

            attr.x = initialX
            attr.y = afterY + margin.top

            afterY = attr.y + attr.height + textRowMargin + margin.bottom + margin.top

            maxX = Math.max(maxX, attr.x + attr.width + textColumnMargin + margin.right)
            maxY = Math.max(maxY, afterY)

            sumHeight += attr.height
          }

          if (_backgroundEle) {
            // 额外的尺寸，表示背景应该比内容稍大一点
            _backgroundEle.setAttr('x', initialX - 2)
            _backgroundEle.setAttr('y', initialY - halfFontHeight - 5)
            _backgroundEle.setAttr('width', Math.max.apply(undefined, elements.map(v => v.attr.width)) + 2)
            _backgroundEle.setAttr('height', sumHeight + 2)
          }

          initialX = maxX

          width = Math.max(width, initialX)
        }
      }

      if (maxY !== 0) {
        initialY = maxY
      }

    }

    return {
      tag: 'g',
      width,
      height: Math.max(initialY, fixedMaxY),
      children: list
    }
  }

  nodeLayout(node) {

    let parentNode = node.parent

    // 若当前节点没有偏移数据，则去继承父节点的偏移数据
    if (!node.translate && parentNode?.translate) {
      node.translate = parentNode.translate
    }

    if (node.layout) {
      return
    }

    // 检查父节点是否已计算布局
    while (parentNode) {
      if (!parentNode.layout) {
        this.nodeLayout(parentNode)
        parentNode = parentNode.parent
      } else {
        break
      }
    }

    const key = getDepthType(node.depth) + 'padding'
    const padding = theme[key]

    let res = this.nodeRender(node)
    res = this.patchNode(res)
    node.layout = res

    const {
      width,
      height
    } = res

    // halfFontHeight 为默认字符渲染后高度的一半
    res.offsetY = -height / 2 + padding.top + this.halfFontHeight - 2
    res.offsetX = padding.left

    const style = {}

    const box = {
      width: width + padding.right + padding.left,
      height: height + padding.top + padding.bottom,
      offsetX: 0
    }
    box.offsetY = -box.height / 2
    style.box = box

    node.style = style

    this.resetNodeX(node)
  }

  updateContent(nodeEnterSelection, nodeUpdateSelection, toggleMode = false) {

    const key = this.key

    let updateSelection

    if (toggleMode) {
      // 切换节点渲染模式时，移除更新节点的原先的内容
      nodeUpdateSelection.selectAll(NODE_CONTAINER.selector).remove()
      updateSelection = nodeUpdateSelection.merge(nodeEnterSelection)
    } else {
      updateSelection = nodeEnterSelection
    }

    updateSelection.selectAll(NODE_CONTAINER.selector)
            .data(d => [d], d => `${key}-${d.data._key}`)
            .enter()
            .append('g')
            .classed(NODE_CONTAINER.cls, true)
            .attr('transform', ({layout}) => `translate(${layout.offsetX}, ${layout.offsetY})`)
            .each(function (datum) {

              let {
                layout: {
                  children,
                },
                data
              } = datum

              const selection = select(this)

              children = children.flat(1)

              for (const item of children) {
                const {_backgroundEle} = item
                if (_backgroundEle) {
                  _backgroundEle.append(selection)
                }

                for (const element of item.elements) {
                  const thisSelection = element.append(selection)
                  for (const [key, listener] of item.listeners) {
                    thisSelection.on(key, function (event) {
                      listener(event, data)
                    })
                  }
                }
              }
            })
  }

  updateSwitchIcon(updateSelection, enterSelection) {
    const {treeMind} = this
    updateSelection.filter(filterSwitchIcon)
            .selectAll(USE_SWITCH_ICON_CLASS.selector)
            .attr('xlink:href', d => `#${d.data._children ? 'add-icon' : 'close-icon'}`)

    const useSwitchSelection = enterSelection.filter(filterSwitchIcon)

    useSwitchSelection.append('use')
            .classed(USE_SWITCH_ICON_CLASS.cls, true)
            .attr('transform', `translate(-12 -6)`)
            .attr('xlink:href', d => `#${d.data._children ? 'add-icon' : 'close-icon'}`)
            .on('click', function (event, node) {
              event.stopPropagation()

              const {_children, children} = node.data
              const len = (children || []).length + (_children || []).length

              if (len === 0) {
                return
              }

              // 隐藏 显示子节点
              if (node.data._children) {
                node.data.children = _children
                node.data._children = null
              } else {
                node.data._children = children
                node.data.children = null
              }

              treeMind.update(node)
            })
  }

  setHalfFontHeight() {
    const {treeMind: {hiddenSvg}, defaultFontStyle} = this

    const fontSize = defaultFontStyle['font-size'] || DEFAULT_FONT_SIZE
    const {height} = getBBox(hiddenSvg, '国', fontSize)
    this.halfFontHeight = height / 2
  }

  updateNode(nodes) {
    const keys = nodes.map(v => v.data._key)
    const updateNodeSelect = this.nodeSelection.filter(v => keys.includes(v.data._key))
    updateNodeSelect.selectAll(NODE_CONTAINER.selector)
            .remove()
    this.updateContent(updateNodeSelect)
  }

  /**
   *
   * @param cb {Function}
   */
  eachNodes(cb) {
    for (const node of this.nodes) {
      cb(node)
    }
  }

  clearSelectedStatus() {
    if (this.selectedNodes.length !== 0) {
      for (const node of this.selectedNodes) {
        node.selected = false
      }

      this.updateBorder()
      treeMind.dispatchSelected([])
    }
  }

  /** 用于框选时候的节点选中状态更新
   *
   * @param selection
   */
  updateSelected(selection) {
    // 框选前，把前一个选中的节点的选中状态全部置为 false
    this.selectedNodes.forEach(node => {
      node.selected = false
    })

    const nodes = getSelectionData(this.nodeSelection)
    const selectedNodes = []
    for (const node of nodes) {
      node.selected = boxContains(selection, node)
      if (node.selected) {
        selectedNodes.push(node)
      }
    }
    this.treeMind.dispatchSelected(selectedNodes)
    this.selectedNodes = selectedNodes
    this.updateBorder()
  }

  getNodeTranslate(d) {
    const translate = d.translate || {x: 0, y: 0}

    let y = d.y + translate.y
    y = this.treeMind.rectifyY(y)

    return `translate(${d.x + translate.x}, ${y})`
  }
}
