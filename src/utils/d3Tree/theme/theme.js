import {formatPadding} from "./themeUtil.js";

const defaultTheme = {
  // 根节点边框颜色
  'root-border-color': '#73A1BF',
  // 根节点边框粗细
  'root-border-stroke-width': 1,
  // 根节点边框选中粗细
  'root-border-selected-stroke-width': 2,
  // 根节点边框搜索中粗细
  'root-border-search-stroke-width': 5,
  // 根节点边框拖拽进入粗细
  'root-border-drop-stroke-width': 4,
  // 根节点填充颜色
  'root-fill': 'rgb(115,161,191)',
  // 根节点文本颜色
  'root-text-color': '#FFFFFF',
  // 根节点文本字体粗细
  'root-text-stroke': 0,
  // 根节点节点内 padding
  'root-padding': [8, 12],

  // 二级节点边框颜色
  'main-border-color': '#73A1BF',
  // 二级节点边框粗细
  'main-border-stroke-width': 1,
  // 二级节点边框选中粗细
  'main-border-selected-stroke-width': 2,
  // 二级节点边框搜索中粗细
  'main-border-search-stroke-width': 5,
  // 二级节点边框拖拽进入粗细
  'main-border-drop-stroke-width': 4,
  // 二级节点填充颜色
  'main-fill': 'rgb(238,243,246)',
  // 二级节点文本颜色
  'main-text-color': '#000000',
  // 二级节点文本字体粗细
  'main-text-stroke': 0,
  // 二级节点节点内 padding
  'main-padding': [8, 12],

  // 其他节点边框颜色
  'sub-border-color': '#73A1BF',
  // 其他节点边框粗细
  'sub-border-stroke-width': 1,
  // 其他节点边框选中粗细
  'sub-border-selected-stroke-width': 2,
  // 他节点边框搜索中粗细
  'sub-border-search-stroke-width': 5,
  // 其他节点边框拖拽进入粗细
  'sub-border-drop-stroke-width': 4,
  // 其他节点填充颜色
  'sub-fill': 'transparent',
  // 其他节点文本颜色
  'sub-text-color': '#000000',
  // 其他节点 padding
  'sub-padding': [8, 12],
  // 文本字体粗细
  'sub-text-stroke': 0,

  // 连线颜色
  'connection-stroke': '#73A1BF',
  // 连线线宽
  'connection-stroke-width': 0.5,

  // switch-icon 线宽
  'switch-icon-stroke-width': 0.5,
  // switch-icon 颜色
  'switch-icon-stroke': 'rgb(128,128,128)',
}

export const theme = formatPadding(defaultTheme)
