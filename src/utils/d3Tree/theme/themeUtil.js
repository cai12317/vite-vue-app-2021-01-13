/** 转换 padding 的函数
 *
 * @param item {number| number[]} 如 1 [1] [1,2] 参照 css
 * @returns {{top: number, left: number, bottom: number, right: number}}
 */
import {theme} from "./theme.js";

export function getPaddingObj(item) {

  if (typeof item === "number") {
    item = [item]
  }
  if (Array.isArray(item)) {
    item = [...item]
    if (item.length === 1) {
      item.length = 4
      item.fill(item[0])
    } else if (item.length === 2) {
      item[2] = item[0]
      item[3] = item[1]
    } else if (item.length === 3) {
      item[3] = item[1]
    }
  }

  return {
    top: item[0],
    right: item[1],
    bottom: item[2],
    left: item[3],
  }
}

/** 把主题转换为能统一识别的对象
 *
 * @param data
 * @returns {*}
 */
export function formatPadding(data) {
  for (const dataKey in data) {
    if (dataKey.endsWith('-padding')) {
      const item = data[dataKey]
      data[dataKey] = getPaddingObj(item)
    }
  }

  return data
}

export const depthType = ['root-', 'main-', 'sub-']

export function getDepthType(depth) {
  return depthType[depth > 2 ? 2 : depth]
}

/** 主题设置
 *
 * @param type {string}
 * @returns {function(*): *}
 */
export function setTheme(type) {
  return d => theme[getDepthType(d.depth) + type]
}

/** border 有多种状态
 * 选中状态 selected = true 优先级1最高
 * 查找状态 search = true 优先级2
 * 默认的不同级别的 border 宽度 优先级3
 *
 * @param type {string} 边框类型，没指定时会自动确定边框类型
 */
export function setBorderStrokeWidth(type = '') {
  return function (node) {
    let _type = type
    if (!_type) {
      if (node.selected) {
        _type = 'border-selected-stroke-width'
      } else if (node.search) {
        _type = 'border-search-stroke-width'
      } else {
        _type = 'border-stroke-width'
      }
    }
    return setTheme(_type)(node)
  }
}
