import {hierarchy,} from 'd3'
import {perfectConfig, searchObject, getSelectionData,} from "./utils.js";
import {initEvent} from "./event/initEvent.js";
import {TreeDispatch, TreeEvent} from "./event/treeDispatch.js";
import {initTreeMind} from "./init.js";
import {initPermission} from "./permission/permission.js";
import {Connection} from "./core/connection.js";
import {ExtraConnection} from "./core/extraConnection.js";
import {NODE_TYPE_ENUM} from "./core/nodeTypeEnum.js";
import {EVENT_TYPE_ENUM} from "./event/eventEnum.js";
import {RequestIdleCallbackUpdate} from "./core/requestIdleCallbackUpdate.js";
import {TreeMindPreview} from "./core/treeMindPreview.js";

let treeMindId = 0

export class TreeMind {

  dom
  data
  config
  treeLayout
  pathBox
  nodeBox
  /**
   * @var {Connection}
   */
  connection
  /**
   * @var {BaseNode}
   */
  baseNode
  /**
   * @var {TreeDispatch}
   */
  dispatch
  permissionInterceptor
  counter = 0
  // 缓存上一次选中的数据
  catchSelected = []
  /**
   * 布局工具
   * @var {{}}
   */
  layoutUtil
  svg
  // 用于记录每个节点的偏移量
  dragNodeMap = new Map()
  /** 当前 svg 是否处于聚焦状态
   * @var {boolean}
   */
  focus = false
  /**
   * @var {number}
   */
  treeMindId
  /**
   * @var {ExtraConnection}
   */
  extraConnection
  /**
   * 用于计算布局的 svg 画布
   */
  hiddenSvg
  /**
   * 最外层盒子的尺寸信息
   */
  boundingClientRect
  /** 当前节点是否能被看到
   * @var {Function}
   */
  inParentBox
  /**
   * @var {RequestIdleCallbackUpdate}
   */
  requestIdleCallbackUpdate
  /**
   * @var {RequestIdleCallbackUpdate}
   */
  requestIdleCallbackSearch
  /** 偏移超过最大值后的矫正值
   *
   * @type {number}
   */
  translateOffsetY = 0
  /** 预览容器
   *
   * @type {TreeMindPreview}
   */
  treeMindPreview

  /**
   *
   * @param dom {Element}
   * @param data
   * @param config {{}|*} 配置
   * @param config.getName {Function} 获取节点文本信息
   * @param config.disabled {boolean} 是否禁用 true 为禁用 默认为 false
   * @param config.upTextKey {string} 线上文本的key 默认 upText
   * @param config.downTextKey {string} 线上文本的key 默认 downText
   * @param config.getKey {Function} 获取节点时的比对方法，若你想要在连线时只传入一个 key 就可以让 TreeMind 找到对应的 Node
   *                                  请为每一条数据指定唯一的 key 指，默认 d => d.id
   * @param config.scaleExtent {Array} 缩放的最大值与最小值 默认 [0.4, 3]
   */
  constructor(dom, data, config = {}) {

    this.dom = dom
    this.data = data
    this.config = perfectConfig(config)
    this.treeMindId = treeMindId++
    this.init()
  }

  init() {
    // 初始化容器
    const {
      svg,
      pathBox,
      nodeBox,
      hiddenSvg,
      boundingClientRect,
    } = initTreeMind(this)

    this.pathBox = pathBox
    this.nodeBox = nodeBox
    this.hiddenSvg = hiddenSvg
    this.boundingClientRect = boundingClientRect

    this.svg = svg
    // 初始化事件监听器
    this.dispatch = new TreeDispatch()
    // 实例化连线
    this.connection = new Connection(pathBox, this)
    // 实例化额外连线
    this.extraConnection = new ExtraConnection(pathBox, this)
    // 实例化节点
    this.initNode(NODE_TYPE_ENUM.DETAIL_NODE)
    // 初始化权限控制
    this.permissionInterceptor = initPermission(this.config)
    // 初始化 zoom 事件； brush 选择器事件
    const layoutUtil = initEvent(this)
    // 初始化分片数据更新器
    this.requestIdleCallbackUpdate = new RequestIdleCallbackUpdate()
    // 初始化分片数据搜索器
    this.requestIdleCallbackSearch = new RequestIdleCallbackUpdate()
    this.treeMindPreview = new TreeMindPreview(this)

    this.disabled(this.config.disabled)
    this.initListeners()

    this.layoutUtil = layoutUtil

    this.inParentBox = (node, beforeNode) => {
      const {width, height} = this.boundingClientRect
      return layoutUtil.inParentBox(width, height, node, beforeNode)
    }

    this.update()
  }

  initListeners() {
    this.dispatch.on(EVENT_TYPE_ENUM.ZOOM_END, () => {
      this.updateNodeDisplay()
    })
  }

  updateSelected(selection) {
    this.baseNode.updateSelected(selection)
  }

  /**
   * 派发节点选中的事件
   */
  dispatchSelected(nodes) {
    let selectedChange
    const currentSelectedKeys = nodes.map(v => v.data._key)

    if (nodes.length !== this.catchSelected.length) {
      selectedChange = true
    } else {
      let mergeSelected = new Set([...currentSelectedKeys, ...this.catchSelected])
      selectedChange = mergeSelected.size !== this.catchSelected.length
    }

    if (selectedChange) {
      this.catchSelected = currentSelectedKeys
      this.dispatch.emit(EVENT_TYPE_ENUM.SELECTED, new TreeEvent(undefined, undefined, {
        nodes: nodes
      }))
    }
  }

  /** 选择所有节点
   *
   * @param all {boolean}
   */
  selectedAll(all = false) {
    if (this.baseNode.selectedNodes.length === 0 && !all) {
      return
    }
    this.baseNode.eachNodes(node => node.selected = all)
    this.baseNode.selectedNodes = all ? this.baseNode.nodes : []
    this.dispatchSelected(this.baseNode.selectedNodes)
    this.baseNode.updateBorder()
  }

  /** 更新节点定位信息
   *
   * @param node 父节点
   * @param offsetX x 偏移
   * @param offsetY y 偏移
   */
  updatePosition(node, offsetX, offsetY) {

    const nodes = getSelectionData(this.baseNode.nodeSelection)

    if (typeof offsetX === "number" && typeof offsetY === "number") {
      for (const n of nodes) {
        let parent = n
        while (parent) {
          if (parent === node) {
            this.setDragNodeMap(n, offsetX, offsetY)
          }
          parent = parent.parent
        }
      }
    }

    this.baseNode.nodeSelection.transition().attr('transform', this.baseNode.getNodeTranslate)

    this.connection.updatePath()
    this.extraConnection.updatePath()
    this.treeMindPreview.update()
  }

  /** 显示隐藏子节点
   *
   * @param node
   * @param visibility
   */
  visibleChildNode(node, visibility = 'hidden') {

    const nodes = getSelectionData(this.baseNode.nodeSelection)

    for (const n of nodes) {
      let parent = n.parent
      while (parent) {
        if (parent === node) {
          n.visibility = visibility
        }
        parent = parent.parent
      }
    }

    this.baseNode.nodeSelection.style('visibility', d => d.visibility)
    this.connection.visiblePath()
    this.extraConnection.visiblePath()
  }

  /**
   *
   * @param source {{}|boolean=} 传入节点对象，表示当更新动画的起点；传 true 表示时更新但是无法确定更新起点，不传表示初始化
   * @param toggleMode {boolean} 更新是否切换了 节点模式
   */
  update(source, toggleMode = false) {
    const isUpdate = !!source

    // d3.hierarchy - 从分层数据构造根节点
    const root = hierarchy(this.data);

    // 将指定的层次结构布局在一个整齐的树中
    this.baseNode.treeLayout(root)

    // 初始化连接线
    this.connection.initLinks(root)
    // 生成后代的数组
    this.baseNode.initNodes(root)
    // 初始化额外的连接线
    this.extraConnection.initLinks(this.baseNode.nodes)

    console.log('------3-----', Date.now())

    const {
      newSource,
      updateLinks,
      updateNodes,
      nodeMap,
    } = this.initializeNodes(source)

    this.baseNode.nodeMap = nodeMap
    this.baseNode.update({newSource, updateNodes, toggleMode})
    this.connection.update({newSource, updateLinks})
    this.extraConnection.update()
    this.treeMindPreview.update()

    console.log('------4-----', Date.now())

    if (!isUpdate) {
      this.location(this.baseNode.nodes[0])
    }
  }

  /** 把节点的初始化放到一个循环中统一处理
   * 1. 交换 x y 坐标
   * 2. 为节点设置内部使用的 _key
   * 3. 更新节点复用以前的偏移数据
   * 4. 设置节点的当前层级顺序
   * 5. 找到更新起点
   * 6. 找到需要渲染的节点 和 连线
   * 7. 节点布局计算
   */
  initializeNodes(source) {
    const isUpdate = !!source

    let order = 0,
            length = null,
            newSource = null,
            findSource = source !== true && !!source,
            updateNodes = [],
            updateLinks = [],
            catchBeforeNode = null,
            nodeMap = Object.create(null)

    if (!findSource) {
      source = this.baseNode.nodes[0]
    }

    for (const node of this.baseNode.nodes) {

      // 交换 x y 坐标
      const _x = node.x
      node.x = node.y
      node.y = _x

      if (typeof node.data._key !== "number") {
        // 为所有数据手动添加 key
        node.data._key = this.counter++
      }

      nodeMap[this.config.getKey(node.data)] = node

      if (isUpdate) {
        // 设置以前的偏移状态
        const translate = this.dragNodeMap.get(node.data._key)
        if (translate) {
          node.translate = translate
        }
      }

      // 为所有节点设置其在当前父节点下的顺序
      if (length === null) {
        length = node.parent ? node.parent.children.length : 1
      }

      node.order = order++

      if (order === length) {
        order = 0
        length = null
      }

      // 找到 source 对应的新node，使用旧的 source 会导致过度动画的初始状态位置错误
      if (findSource && node.data._key === source.data._key) {
        newSource = node
      }

      // 计算出需要渲染的节点
      const beforeNode = node.layout ? null : (catchBeforeNode || node)

      node.display = this.inParentBox(node, beforeNode)

      if (node.display) {
        this.baseNode.nodeLayout(node)
        updateNodes.push(node)

        if (node.parent?.display) {
          updateLinks.push({
            source: node.parent,
            target: node
          })
        }
      }

      if (node.layout) {
        catchBeforeNode = node
      }
    }

    return {
      newSource,
      updateNodes,
      updateLinks,
      nodeMap,
    }
  }

  /**
   * 操作禁用
   * @param disabled {boolean}
   */
  disabled(disabled) {
    this.config.disabled = disabled === undefined ? !this.config.disabled : !!disabled
    this.svg.attr('cursor', this.config.disabled ? 'pointer' : 'auto')
  }

  /**
   *
   * @param node
   * @param x {number}
   * @param y {number}
   * @param accumulative 偏移是否累计
   */
  setDragNodeMap(node, x, y, accumulative = true) {
    const {_key} = node.data
    const item = this.dragNodeMap.get(_key) || {x: 0, y: 0}
    if (accumulative) {
      item.x += x
      item.y += y
    } else {
      item.x = x
      item.y = y
    }
    this.dragNodeMap.set(_key, item)
    node.translate = item
  }

  /** 添加节点
   *
   * @param source 父节点
   * @param data {{}|[]} 子节点数据
   */
  addNode(source, data) {
    if (!Array.isArray(data)) {
      data = [data]
    }

    for (const datum of data) {
      datum._key = this.counter++
      // 记录父节点的偏移数据
      datum.translate = source.translate
    }

    const children = source.data.children || source.data._children || []
    children.push(...data)

    if (source.data._children) {
      source.data._children = null
    }

    source.data.children = children

    this.update(source)
  }

  /**
   * 获取当前选中的节点
   */
  getSelected() {
    return this.baseNode.selectedNodes
  }

  /** 移除节点，不传默认会移除当前选中的节点
   *
   * @param nodes {{}|[]|undefined}
   */
  deleteNodes(nodes) {
    nodes = nodes || this.getSelected()

    if (!Array.isArray(nodes)) {
      nodes = [nodes]
    }

    // 按照降序排序，必须先删除 order 较大的节点
    nodes.sort((a, b) => b.order - a.order)

    for (const node of nodes) {
      if (!node.parent) {
        continue
      }

      const parentData = node.parent.data
      parentData.children.splice(node.order, 1)
    }

    this.update(true)
  }

  /**
   * 显示隐藏连线上的文本
   */
  visibleConnectionText() {
    this.connection.visibleConnectionText()
  }

  getNodeByKey(node) {
    if (typeof node === "string" || typeof node === "number") {
      let key = node
      node = this.baseNode.nodeMap[node]

      if (!node) {
        console.error(`未找到 key 为 ${key} 的数据`)
      }

    }
    return node
  }

  /** 创建额外的连线
   *
   * @param source {string|number|{}} 来源节点
   * @param target {string|number|{}} 目的节点
   * @param style {{}} 连线样式
   * @param style.stroke {string} 连线颜色
   * @param style.strokeWidth {string} 连线宽度
   */
  createExtraConnection(source, target, style) {

    source = this.getNodeByKey(source)
    target = this.getNodeByKey(target)

    if (source && target) {
      this.extraConnection.createConnection(source, target, style)
      this.extraConnection.update()
    }
  }

  batchCreateExtraConnection(list) {
    for (let {source, target, style} of list) {
      source = this.getNodeByKey(source)
      target = this.getNodeByKey(target)

      if (source && target) {
        this.extraConnection.createConnection(source, target, style)
      }
    }
    this.extraConnection.update()
  }

  /**
   *
   * @param nodeClass {BaseNode}
   */
  initNode(nodeClass = NODE_TYPE_ENUM.COMMON_NODE) {
    this.baseNode = new nodeClass(this.nodeBox, this)
  }

  toggleNodeRenderMode() {

    const nodeClass = this.baseNode instanceof NODE_TYPE_ENUM.COMMON_NODE ? NODE_TYPE_ENUM.DETAIL_NODE : NODE_TYPE_ENUM.COMMON_NODE

    const root = this.baseNode.nodes[0]
    this.initNode(nodeClass)
    this.update(root, true)
  }

  /** 节点数据更新 两种更新方式
   *
   *  updateNode(node, data)
   *
   *  updateNode([{node, data}, {node: node1, data: data1}])
   *
   * @param node {[]|{}} 修改数据的节点
   * @param data {{}} 数据
   */
  updateNode(node, data) {
    let nodes
    if (!Array.isArray(node)) {
      nodes = [
        {
          node,
          data
        }
      ]
    } else {
      nodes = node
    }

    for (const {node, data} of nodes) {
      Object.assign(node.data, data)
    }

    const root = this.baseNode.nodes[0]
    this.update(root)
    this.baseNode.updateNode(nodes.map(v => v.node))
  }

  /** 节点搜索
   *
   *  search({name: 'string', text: 'string'})
   *
   *  search(data => data.name === 'string')
   *
   * @param value {Function|{}}
   * @param option {{}}
   * @param option.checked {boolean} 是否为搜索到的节点添加加粗的边框
   * @param option.resultCallback {Function} 搜索完成的回调
   */
  search(value, option = {}) {
    const {checked = true, resultCallback} = option
    if (typeof value !== "function") {
      const compareData = value
      value = data => searchObject(data, compareData)
    }

    const searchResult = []
    let locationIndex = 0

    const locationNext = () => {
      if (searchResult.length !== 0) {

        if (locationIndex === searchResult.length) {
          locationIndex = 0
        }

        console.log(searchResult[locationIndex])
        this.location(searchResult[locationIndex++])
        console.log(searchResult[locationIndex])
      }
    }

    const eachCallback = node => {
      if (value(node.data)) {
        searchResult.push(node)
        node.search = true

        if (searchResult.length === 1) {
          locationNext()
        }

      } else {
        node.search = false
      }
    }

    const eachAfterCallback = () => {
      this.baseNode.updateBorder()
    }

    const updateCallback = () => {
      eachAfterCallback()
      resultCallback && resultCallback(searchResult)
    }

    this.requestIdleCallbackSearch.useFragmentUpdate(this.baseNode.nodes, eachCallback, updateCallback, eachAfterCallback)

    return {
      // 在超过 OPEN_REQUEST_IDLE_CALLBACK_DATA_LENGTH 长度的数据搜索时，
      // 不要相信 searchResult 在 return 时的返回值，请到 resultCallback 内去接收搜索结果
      searchResult,
      locationNext,
      cancelSearch: this.requestIdleCallbackSearch.cancel.bind(this.requestIdleCallbackSearch)
    }
  }

  location(node) {
    this.baseNode.nodeLayout(node)

    let {
      x: left,
      y: top,
      style: {
        box: {
          width,
          height,
          offsetY,
        }
      }
    } = node

    top -= height / 2
    top -= offsetY

    const {
      width: outBoxWidth,
      height: outBoxHeight
    } = this.boundingClientRect

    const translateX = (outBoxWidth - width) / 2 - left
    const translateY = (outBoxHeight - height) / 2 - top

    this.layoutUtil.nodeLocation(translateX, translateY)
  }

  clearSearchStatus() {
    this.baseNode.eachNodes(node => node.search = false)
    this.baseNode.updateBorder()
    this.dispatch.emit(EVENT_TYPE_ENUM.CLEAR_SEARCH_STATUS)
  }

  updateNodeDisplay(source) {
    let catchBeforeNode, updateNodes = [], updateLinks = []

    const eachCallback = node => {
      node.display = this.inParentBox(node, node.layout ? null : (catchBeforeNode || node))

      if (node.display) {
        this.baseNode.nodeLayout(node)
        updateNodes.push(node)

        if (node.parent?.display) {
          updateLinks.push({
            source: node.parent,
            target: node
          })
        }
      }
      if (node.layout) {
        catchBeforeNode = node
      }
    }

    const updateCallback = () => {
      this.baseNode.update({source, updateNodes})
      this.connection.update({source, updateLinks})
      this.extraConnection.update()
      this.treeMindPreview.update()
    }

    this.requestIdleCallbackUpdate.useFragmentUpdate(this.baseNode.nodes, eachCallback, updateCallback)
  }

  rectifyY(y, opposite = false) {
    if (this.translateOffsetY) {
      const condition = opposite ? y < 0 : y > 0

      if (condition) {
        y -= this.translateOffsetY
      } else {
        y += this.translateOffsetY
      }
      return y
    } else {
      return y
    }
  }

  /** 画布缩放
   *
   * @param k {number} 缩放值， 值需再 config.scaleExtent 区间内
   */
  scale(k) {
    const [min, max] = this.config.scaleExtent
    if (k < min) {
      k = min
    } else if (k > max) {
      k = max
    }

    this.svg.transition()
            .call(this.layoutUtil.zoomCall.scaleTo, k)
  }
}
