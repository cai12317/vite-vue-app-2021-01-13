import {select} from "d3";
import {theme} from "./theme/theme.js";
import {BOX_MIX_WIDTH, BOX_MIX_HEIGHT, ROOT_BOX_CLASS} from "./core/constants.js";

/** 清除容器内容
 *
 * @param dom {Element}
 */
export function clearContainer(dom) {
  if (dom.innerHTML) {
    dom.innerHTML = ''
  }
}

/** 创建节点的外边框路径
 *
 * @param height {number} 容器高度
 * @param width {number} 宽度
 * @param borderRadios {number} 圆角 默认 4
 * @param c
 * @returns {string} path d
 */
export function createBoxPath(height, width, borderRadios = 4, c = 2) {
  const sideWidth = width - borderRadios * 2
  const sideHeight = height - borderRadios * 2

  return `M ${borderRadios} 0 L ${borderRadios + sideWidth} 0 C ${borderRadios + sideWidth + c} 0 ${width} ${c} ${width} ${borderRadios} L ${width} ${sideHeight + borderRadios} C ${width} ${sideHeight + borderRadios + 2} ${width - c} ${height} ${sideWidth + borderRadios} ${height} L ${borderRadios} ${height} C ${c} ${height} 0 ${height - c} 0 ${height - borderRadios} L 0 ${borderRadios} C 0 ${c} ${c} 0 ${borderRadios} 0 Z`
}

/** 获取容器尺寸
 *
 * @param dom {Element}
 */
export function getContainerSize(dom) {
  const boundingClientRect = dom.getBoundingClientRect()
  const {
    width,
    height,
  } = boundingClientRect

  return {
    width: width > BOX_MIX_WIDTH ? width : BOX_MIX_WIDTH,
    height: height > BOX_MIX_HEIGHT ? height : BOX_MIX_HEIGHT,
    boundingClientRect
  }
}

/** 添加根节点容器
 *
 * @param dom {Element}
 * @param width {number}
 * @param height {number}
 */
export function appendRootSvg(dom, width, height) {
  return select(dom).append('svg')
          .attr('width', width)
          .attr('height', height)
          .attr('style', 'user-select: none;')
          .attr('viewBox', `0 0 ${width} ${height}`)
          .style('position', 'absolute')
          .style('top', '0')
          .style('left', '0')
}

/** 添加根容器
 *
 * @param svg
 */
export function appendRootBox(svg) {
  return svg.append('g')
          .classed(ROOT_BOX_CLASS.cls, true)
}

/** 设置容器的基础样式
 *
 * @param dom {Element}
 */
export function setContainerBaseStyle(dom) {
  select(dom).style('position', 'relative')
          .style('overflow', 'hidden')
          .style('width', '100%')
          .style('height', '100%')
}

export function appendHiddenSvg(dom) {
  return select(dom).append('svg')
          .attr('width', 10)
          .attr('height', 10)
          .attr('viewBox', '0 0 10 10')
          .style('visibility', 'hidden')
          .style('position', 'absolute')
          .style('top', '0')
          .style('right', '-10px')
}

/** SwitchIcon 的选集过滤函数
 *
 * @param d
 * @returns {boolean}
 */
export function filterSwitchIcon(d) {
  return !d.data.root && (d.data.children || []).length + (d.data._children || []).length > 0
}

/** 是否是根节点
 *
 * @param node
 * @returns {boolean} true 是； false 不是
 */
export function isRoot(node) {
  return node.depth === 0
}

export function boxContains([[x0, y0], [x1, y1]], d) {
  const rect = {
    left: x0,
    right: x1,
    top: y0,
    bottom: y1
  }
  const boxRect = getBoxRect(d)
  return checkIntersect(rect, boxRect)
}

/** 证明两个矩形相交
 * https://zhuanlan.zhihu.com/p/29704064
 *
 * @param RectA
 * @param RectB
 * @returns {boolean}
 */
function checkIntersect(RectA, RectB) {
  const nonIntersect = (RectB.right < RectA.left) ||
          (RectB.left > RectA.right) ||
          (RectB.bottom < RectA.top) ||
          (RectB.top > RectA.bottom)
  //相交
  return !nonIntersect
}

function getBoxRect(d) {
  const {style = {}, translate = {x: 0, y: 0}} = d
  const {box = {height: 0, width: 0}} = style
  // 计算四个顶点的坐标
  const heightHalf = box.height / 2
  const x = d.x + translate.x
  const y = d.y + translate.y
  return {
    left: x,
    right: x + box.width,
    top: y - heightHalf,
    bottom: y + heightHalf
  }
}

/** 获取节点的定位及尺寸信息
 *
 * @param x
 * @param y
 * @param translate
 * @param style
 */
export function getNodeRect({x, y, translate, style}) {
  if (translate) {
    y = y + translate.y
    x = x + translate.x
  }

  const box = style?.box || {}

  return {x, y, ...box}
}

/** 完善配置
 *
 */
export function perfectConfig(config = {}) {
  const {
    getName = v => v.name,
    upTextKey = 'upText',
    downTextKey = 'downText',
    getKey = v => v.id,
    disabled = false,
    scaleExtent = [0.4, 3],
  } = config

  return {
    getName,
    upTextKey,
    downTextKey,
    getKey,
    disabled,
    scaleExtent,
  }
}

/** 获取用于创建额外连线的 node 定位信息
 *
 * @param source
 * @param target
 */
export function getExtraDiagonalNode(source, target) {

  // 判断 target 相对于 source 的方位

  const {x, y, width, height} = getNodeRect(source)
  const {x: x_, y: y_, width: width_, height: height_} = getNodeRect(target)

  const sourceOnTop = y > y_

  return {
    source: {
      y: y + (height * (sourceOnTop ? -1 : 1)) / 2,
      x: x + width / 2
    },
    target: {
      y: y_ + (height_ * (sourceOnTop ? 1 : -1)) / 2,
      x: x_ + width_ / 2
    },
  }
}

export function getBBox(parentSvg, text, fontSize) {
  const textEle = parentSvg.append('text')
          .text(text)
          .attr('font-size', fontSize)
  const BBox = textEle.node().getBBox()
  textEle.remove()
  return BBox
}

export function searchObject(target, data) {
  return Object.keys(data).every(key => data[key] === target[key])
}

/** 获取当前选集的数据集
 *
 * @param selection
 * @returns {*[]}
 */
export function getSelectionData(selection) {
  if (selection) {
    const data = []
    selection.each(d => {
      data.push(d)
    })
    return data
  } else {
    return []
  }
}

export function getLinkKey({target, source}) {
  return `${target.data._key}-${source.data._key}`
}

export function getNodeTranslate(d) {
  const translate = d.translate || {x: 0, y: 0}
  return `translate(${d.x + translate.x}, ${d.y + translate.y})`
}

/** 节流函数
 *
 * @param fn {Function}
 * @param time {number}
 */
export function throttle(fn, time = 500) {
  let params = [], timeout
  return function (...args) {
    params = args

    if (timeout) {
      return
    }

    timeout = setTimeout(() => {
      fn(...params)
      timeout = null
    }, time)
  }
}
