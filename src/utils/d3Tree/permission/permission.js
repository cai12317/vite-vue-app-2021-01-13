export function initPermission(config) {
  /**
   * 权限拦截器
   */
  return function permissionInterceptor(callback, type = null) {
    if (config.disabled) {
      return
    }
    callback()
  }
}
