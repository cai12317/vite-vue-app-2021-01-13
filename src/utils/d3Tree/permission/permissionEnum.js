export const PERMISSION_HANDLER_ENUM = {
  // 节点点击
  'NODE_CLICK': 'NODE_CLICK',
  // 节点右键事件
  'NODE_CONTEXTMENU': 'NODE_CONTEXTMENU',
  'ICON_CLICK': 'ICON_CLICK',
  // 拖拽事件
  'DRAG': 'DRAG',
  // 框选事件
  'BRUSH': 'BRUSH',
  // 节点双击
  'NODE_DBLCLICK': 'NODE_DBLCLICK',
  'DELETE': 'DELETE',
}
