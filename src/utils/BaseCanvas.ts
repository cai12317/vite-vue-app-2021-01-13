import {Shape} from "@/utils/Shape";
import {AndShape} from "@/utils/Drawing";
import {EventCanvas} from "@/utils/EventCanvas";

export class BaseCanvas {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  width: number
  height: number
  shapes: AndShape[];
  revocationShapes: AndShape[];
  eventCanvas?: EventCanvas;

  constructor(canvas: HTMLCanvasElement, width: number, height: number) {
    const ctx = canvas.getContext('2d')
    this.canvas = canvas
    this.ctx = ctx as CanvasRenderingContext2D
    this.width = width
    this.height = height
    this.shapes = []
    this.revocationShapes = []
  }

  clearRect() {
    this.ctx.clearRect(0, 0, this.width, this.height)
  }

  reDraw() {
  }

  /**
   * 撤销上次绘图
   */
  revocation() {
    if (this.shapes.length > 0) {
      this.revocationShapes.push(this.shapes.pop() as Shape)
      this.reDraw()

      if (this.eventCanvas) {
        this.eventCanvas.revocation()
      }
    }
  }

  /**
   * 恢复上一次撤销
   */
  recover() {
    if (this.revocationShapes.length > 0) {
      this.shapes.push(this.revocationShapes.pop() as Shape)
      this.reDraw()

      if (this.eventCanvas) {
        this.eventCanvas.recover()
      }
    }
  }
}
