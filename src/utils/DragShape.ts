import {Point} from "@/utils/Point";

export interface InterfaceDragShape {
  drawable?: boolean
  dragStartPoint: Point | null

  dragStart(x: number, y: number): boolean

  dragging(x: number, y: number): boolean

  dragEnd(x: number, y: number): boolean
}
