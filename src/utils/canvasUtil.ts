const RGB = [0, 0, 1]
let RGBMax = 255
export const SHAPE_ID_JOIN_SYMBOL = ','

/**
 * 获取图形 Id , 使用 rgb 颜色进行拼接：125-325-214
 */
export function getShapeId(): string {
  while (true) {
    if (RGB[2] <= RGBMax) {
      const res = RGB.join(SHAPE_ID_JOIN_SYMBOL)
      RGB[2] += 1
      return res
    }

    if (RGB[2] > RGBMax) {
      RGB[2] = 0
      RGB[1] += 1

      if (RGB[1] > RGBMax) {
        RGB[1] = 0
        // 图形数量应该不可能超过 RGB[0] > 255
        RGB[0] += 1
      }
    }
  }
}

export function shapeIdTransformToRGBA(shapeId: string) {
  return shapeId.split(SHAPE_ID_JOIN_SYMBOL).map(Number)
}
