import {getShapeId} from "@/utils/canvasUtil";
import {InterfaceDragShape} from "@/utils/DragShape";
import {Point} from "@/utils/Point";

export interface InterfaceShape {
  id: string
  ctx: CanvasRenderingContext2D;

  strokeStyle?: string | CanvasGradient | CanvasPattern;
  lineWidth?: number;
  fillStyle?: string | CanvasGradient | CanvasPattern;

  draw(activeShapeId: string): void

  drawEventCanvas(ctx: CanvasRenderingContext2D): void

  playback(): void

}

export class Shape implements InterfaceShape, InterfaceDragShape {

  id: string;
  ctx: CanvasRenderingContext2D;
  strokeStyle: string | CanvasGradient | CanvasPattern;
  lineWidth: number;
  fillStyle?: string | CanvasGradient | CanvasPattern;
  dragStartPoint: Point | null;

  constructor(ctx: CanvasRenderingContext2D, x: number, y: number) {
    this.id = getShapeId()
    this.ctx = ctx
    this.strokeStyle = ctx.strokeStyle
    this.lineWidth = ctx.lineWidth
    this.dragStartPoint = null
  }

  drawEnd(x: number, y: number): Shape | void {
    return this
  }

  drawMove(x: number, y: number): Shape {
    return this
  }

  draw(activeShapeId: string = '', ctx?: CanvasRenderingContext2D): void {
  }

  playback(): void {
  }

  drawEventCanvas(ctx: CanvasRenderingContext2D): void {
    this.draw(undefined, this.ctx)
  }

  dragEnd(x: number, y: number): boolean {
    return false;
  }

  dragStart(x: number, y: number): boolean {
    return false;
  }

  dragging(x: number, y: number): boolean {
    return false;
  }
}
