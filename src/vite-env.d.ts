/// <reference types="vite/client" />

// 这行代码处理 webstorm 中的 .ts 文件中的报错
// Cannot find module '../views/HomeView.vue' or its corresponding type declarations.
// 在 使用 npm create vite 创建的项目中也存在这个问题，且下面代码的处理方式会导致点击对应组件无法进行跳转到对应代码，没有找到其他更加完美的处理办法

declare module "*.vue" {
  import Vue from "@/vue";
  export default Vue;
}


declare interface ImportMetaEnv {
  readonly VITE_DEFINE_TEST: string
}