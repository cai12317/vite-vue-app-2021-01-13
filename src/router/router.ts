import {createRouter, createWebHashHistory, RouteRecordRaw} from 'vue-router';
import DefineTest from '@/view/DefineTest.vue'

export const routes: RouteRecordRaw[] = [
  {
    name: 'Home',
    path: '/',
    component: () => import('@/view/Home.vue'),
    meta: {
      title: '首页'
    }
  },
  {
    name: 'TransitionGroupTest',
    path: '/transition-group-test',
    component: () => import('@/view/TransitionGroupTest.vue'),
    meta: {
      title: '列表过渡'
    }
  },
  {
    name: 'VirtualList',
    path: '/virtual-list',
    component: () => import('@/view/VirtualList.vue'),
    meta: {
      title: '虚拟列表'
    }
  },
  {
    name: 'GridTest',
    path: '/grid-test',
    component: () => import('@/view/GridTest.vue'),
    meta: {
      title: 'Grid网格列表'
    }
  },
  {
    name: 'EasyFLIPTest',
    path: '/easy-FLIP-test',
    component: () => import('@/view/EasyFLIPTest.vue'),
    meta: {
      title: '简单FLIP动画实验'
    }
  },
  {
    name: 'FLIPTest',
    path: '/FLIP-test',
    component: () => import('@/view/FLIPTest.vue'),
    meta: {
      title: 'FLIP动画实验'
    }
  },
  {
    name: 'EventLoopTest',
    path: '/event-loop-test',
    component: () => import('@/view/EventLoopTest.vue'),
    meta: {
      title: 'eventLoop实验'
    }
  },
  {
    name: 'Css3DWebpack',
    path: '/css-3d-webpack',
    component: () => import('@/view/Css3DWebpack.vue'),
    meta: {
      title: 'Css3DWebpack实验'
    }
  },
  {
    name: 'CssFilter',
    path: '/css-filter',
    component: () => import('@/view/CssFilter.vue'),
    meta: {
      title: 'CssFilter实验'
    }
  },
  {
    name: 'Css3DTreeJs',
    path: '/css-3d-tree-js',
    component: () => import('@/view/Css3DTreeJs.vue'),
    meta: {
      title: 'Css3DTreeJs实验'
    }
  },
  {
    name: 'TreeJsDemo',
    path: '/tree-js-demo',
    component: () => import('@/view/TreeJsDemo.vue'),
    meta: {
      title: 'TreeJsDemo实验'
    }
  },
  {
    name: 'CanvasTest',
    path: '/canvas-test',
    component: () => import('@/view/CanvasTest.vue'),
    meta: {
      title: 'Canvas实验'
    }
  },
  {
    name: 'LottieWeb',
    path: '/lottie-web',
    component: () => import('@/view/LottieWeb.vue'),
    meta: {
      title: 'LottieWeb实验'
    }
  },
  {
    name: 'Watermark',
    path: '/watermark',
    component: () => import('@/view/Watermark.vue'),
    meta: {
      title: 'Watermark实验'
    }
  },
  {
    name: 'TransformAnimation',
    path: '/transform-animation',
    component: () => import('@/view/TransformAnimation.vue'),
    meta: {
      title: 'animation-timing-function实验'
    }
  },
  {
    name: 'MenuBoom',
    path: '/menu-boom',
    component: () => import('@/view/MenuBoom.vue'),
    meta: {
      title: '环绕菜单动画'
    }
  },
  {
    name: 'CssProperty',
    path: '/css-property',
    component: () => import('@/view/CssProperty.vue'),
    meta: {
      title: 'CssProperty'
    }
  },
  {
    name: 'D3Test',
    path: '/d3Test',
    component: () => import('@/view/D3Test.vue'),
    meta: {
      title: 'D3实验'
    }
  },
  {
    name: 'D3Tree',
    path: '/d3Tree',
    component: () => import('@/view/D3Tree.vue'),
    meta: {
      title: 'D3Tree'
    }
  },
  {
    name: 'D3RadialTree',
    path: '/d3RadialTree',
    component: () => import('@/view/D3RadialTree.vue'),
    meta: {
      title: 'D3RadialTree'
    }
  },
  {
    name: 'CSSSnowAnimation',
    path: '/CSSSnowAnimation',
    component: () => import('@/view/CSSSnowAnimation.vue'),
    meta: {
      title: 'CSS下雪动画'
    }
  },
  {
    name: 'Test',
    path: '/test',
    component: () => import('@/view/Test.vue'),
    meta: {
      title: 'Test'
    }
  },
]
console.log(import.meta.env.VITE_DEFINE_TEST)
if (import.meta.env.VITE_DEFINE_TEST === 'true') {
  routes.push(
          {
            name: 'DefineTest',
            path: '/defineTest',
            component: DefineTest,
            meta: {
              title: 'DefineTest'
            }
          },)
}


export const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

router.beforeEach((to, _from, next) => {
  document.title = (to.meta.title || 'Vite App') as string
  next()
})
